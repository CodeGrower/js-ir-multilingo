import { Controller, Param, Put, Body, ValidationPipe, Get, UseGuards } from '@nestjs/common';
import { ArticleVersionService } from './article-version.service';
import { ShowArticleDTO } from '../articles/models/showArticleDTO';
import { InjectUser } from '../articles/articles.controller';
import { User } from '../data/enitites/user.entity';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { ArticleUpdateDTO } from '../articles/models/ArticleUpdateDTO';
import { ArticleVersionDTO } from '../articles/models/ArticleVersionDTO';

@Controller('article-version')
@UseGuards(AuthGuardWithBlacklisting)
export class ArticleVersionController {
    constructor(
        private readonly articleVersionService: ArticleVersionService,
    ) {}

    @Put('/:articleId/delete')
    async deleteArticleVersion(@Param('articleId') articleId: string) {
        return this.articleVersionService.deleteArticleVersion(articleId);
    }

    @Put('/:articleId')
    async updateArticleVersion(
        @Body(new ValidationPipe({ whitelist: true, transform: true })) body: any, @Param('articleId') articleId: string): Promise<ShowArticleDTO> {
        return await this.articleVersionService.updateArticleVersion(articleId, body);
    }

    @Put('/:articleId/current')
    async setCurrentArticleVersion(@Param('articleId') articleId: string): Promise<ShowArticleDTO> {
        return this.articleVersionService.setCurrentArticleVersion(articleId);
    }

    @Get('/:articleId/versions')
    async getAllArticleVersions(@Param('articleId') articleId: string): Promise<ArticleVersionDTO[]> {
        return this.articleVersionService.getAllArticleVersions(articleId);
    }
    @Get('/current/articles')
    async getAllCurrentArticles(): Promise<ShowArticleDTO[]> {
        return this.articleVersionService.getAllCurrentArticles();
    }

    @Get('/user/articles/versions')
    async getAllUserArticlesVersions(@InjectUser() user: User): Promise<ArticleVersionDTO[]> {
        return this.articleVersionService.getAllUserArticlesVersions(user.id);
    }

    @Get('/:articleId')
    async getArticle(@Param('articleId') articleId: string): Promise<ArticleUpdateDTO> {
        return await this.articleVersionService.getArticle(articleId);
    }
}
