import { Module } from '@nestjs/common';
import { ArticleVersionService } from './article-version.service';
import { ArticleVersionController } from './article-version.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VersionArticle } from '../data/enitites/article-version.entity';
import { TranslationService } from '../article-translation/translation.service';
import { Language } from '../data/enitites/languages.entity';
import { User } from '../data/enitites/user.entity';
import { TranslationRating } from '../data/enitites/rate.entity';
import { TranslationModule } from '../article-translation/translation.module';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { RateModule } from '../translation-rate/rate.module';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Module({
  imports: [TypeOrmModule.forFeature([VersionArticle, Language, TranslationArticle, User, TranslationRating]), TranslationModule, RateModule],
  providers: [ArticleVersionService, TranslationService, CommonServiceService],
  controllers: [ArticleVersionController],
  exports: [ArticleVersionService],
})
export class ArticleVersionModule {}
