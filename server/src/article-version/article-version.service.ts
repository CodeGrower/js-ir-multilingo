import { Injectable } from '@nestjs/common';
import { CreateArticleDTO } from '../articles/models/createArticleDTO';
import { InjectRepository } from '@nestjs/typeorm';
import { VersionArticle } from '../data/enitites/article-version.entity';
import { Repository } from 'typeorm';
import { Article } from '../data/enitites/article.entity';
import { ArticleDTO } from '../articles/models/ArticleDTO';
import { ShowArticleDTO } from '../articles/models/showArticleDTO';
import { TranslationService } from '../article-translation/translation.service';
import { User } from '../data/enitites/user.entity';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { plainToClass } from 'class-transformer';
import { ShowArticleByIdDTO } from '../articles/models/ShowArticleByIdDTO';
import { ArticleUpdateDTO } from '../articles/models/ArticleUpdateDTO';
import { ArticleVersionDTO } from '../articles/models/ArticleVersionDTO';

// tslint:disable-next-line: no-var-requires
const {Translate} = require('@google-cloud/translate').v2;
const translate = new Translate();

@Injectable()
export class ArticleVersionService {
    constructor(
        @InjectRepository(VersionArticle) private readonly versionRepository: Repository<VersionArticle>,
        private readonly translationService: TranslationService,
    ) {}

    async createArticleOriginalVersion(body: CreateArticleDTO, article: Article, user: User, origLang: string): Promise<ShowArticleDTO> {
        const newVersionArticle: VersionArticle = await this.setArticle(body, article.id, origLang, user);
        newVersionArticle.createdOn = new Date().toLocaleString();
        newVersionArticle.updatedOn = new Date().toLocaleString();
        await this.versionRepository.save(newVersionArticle);
        return plainToClass(ShowArticleDTO, newVersionArticle, {
            excludeExtraneousValues: true,
        });
    }

    async updateArticleVersion(artId: string, body: ArticleDTO): Promise<ShowArticleDTO> {
        const currentArticle: any = await this.versionRepository.findOne({where: {id: artId, isCurrent: true, isDeleted: false}, order: {
            version: 'DESC',
        }, relations: ['user', 'article']});
        if (currentArticle === undefined) {
            throw new GlobalSystemError('No article found!', 404);
          }
        currentArticle.isCurrent = false;
        this.versionRepository.save(currentArticle);
        const content = currentArticle.content;
        const origs = await this.translationService.getTranslationByText(content);
        const updatedArticle: VersionArticle = await this.setArticle(
            body, currentArticle.__article__.id, origs.originalLanguage , currentArticle.__user__);
        updatedArticle.imgUrl = currentArticle.imgUrl;
        updatedArticle.version = currentArticle.version + 1;
        updatedArticle.createdOn = currentArticle.createdOn;
        updatedArticle.updatedOn = new Date().toLocaleString();
        const newArticle: VersionArticle = await this.versionRepository.save(updatedArticle);
        return plainToClass(ShowArticleDTO, newArticle, {
            excludeExtraneousValues: true,
        });
    }

    async setArticle(body: any, artId: any, origLang: string, user?: User): Promise<VersionArticle> {
        const updatedArticle: VersionArticle = this.versionRepository.create();
        this.translationService.translateText(body.title, origLang);
        this.translationService.translateText(body.content, origLang);
        updatedArticle.imgUrl = body.imgUrl;
        updatedArticle.content = body.content;
        updatedArticle.title = body.title;
        updatedArticle.article = Promise.resolve(artId);
        updatedArticle.version = 1,
        updatedArticle.isCurrent =  true;
        updatedArticle.user = Promise.resolve(user);
        return updatedArticle;
    }

    async setCurrentArticleVersion(Id: string): Promise<ShowArticleDTO> {
        const currentArticleVersion: VersionArticle = await this.versionRepository.findOne({where: {id: Id}, relations: ['user', 'article']});
        if (currentArticleVersion === undefined) {
            throw new GlobalSystemError('No article found!', 404);
        }
        const originalArticleId = (await currentArticleVersion.article).id;
        const allVersionsToFalse: VersionArticle[] = await this.versionRepository.find({where: {article: originalArticleId}});
        allVersionsToFalse.map(async (article) => {
            article.isCurrent = false;
            await this.versionRepository.save(article);
        });
        currentArticleVersion.isCurrent = true;
        await this.versionRepository.save(currentArticleVersion);
        return plainToClass(ShowArticleDTO, currentArticleVersion, {
            excludeExtraneousValues: true,
        });
    }

    async getAllArticleVersions(artId: string): Promise<ArticleVersionDTO[]> {
        const allVersions: VersionArticle[] = await this.versionRepository.find({where: {article: artId, isDeleted: false}, relations: ['article']});
        if (allVersions === undefined || allVersions.length < 0) {
            throw new GlobalSystemError('No versions available for this article!', 404);
          }
        allVersions.map(async (article) => {
            article.isCurrent = false;
            await this.versionRepository.save(article);
        });
        return plainToClass(ArticleVersionDTO, allVersions, {
            excludeExtraneousValues: true,
        });

    }
    async getAllArticles(langCode: string): Promise<VersionArticle[]> {
        const allCurrentArticles: VersionArticle[] = await this.versionRepository.find(
            {where: {isCurrent: true, isDeleted: false}, relations: ['user'], order: {
            createdOn: 'DESC',
        }});
        if (allCurrentArticles === undefined) {
            throw new GlobalSystemError('No current articles found found!', 404);
          }
        const allArticles: VersionArticle[] = await this.translationService.getAllArticlesByLang(allCurrentArticles, langCode);
        return allArticles;
    }

    async getLastThreeArticles(lang: string): Promise<VersionArticle[]> {
        const allCurrentArticles: VersionArticle[] = await this.versionRepository.find(
            {where: {isCurrent: true, isDeleted: false}, relations: ['user'], order: {
            createdOn: 'DESC',
        }, take: 3,
    });
        let allArticles;
        let articles;
        if (allCurrentArticles === undefined) {
            articles = await this.versionRepository.find({where: {isCurrent: true, isDeleted: false}, relations: ['user'], order: {
                createdOn: 'DESC',
            },
        });
            allArticles = await this.translationService.getAllArticlesByLang(articles, lang);
          } else {
              allArticles = await this.translationService.getAllArticlesByLang(allCurrentArticles, lang);
          }
        return allArticles;
    }

    async getAllUserArticlesVersions(userId: string): Promise<ArticleVersionDTO[]> {
        const userArticles: VersionArticle[] = await this.versionRepository.find(
            {where: {user: userId, isDeleted: false, isCurrent: true}, relations: ['article']});
        if (userArticles === undefined) {
            throw new GlobalSystemError('No articles found for this user!', 404);
          }
        return plainToClass(ArticleVersionDTO, userArticles, {
            excludeExtraneousValues: true,
        });
    }

    async getAllCurrentArticles(): Promise<ShowArticleDTO[]> {
        const showAllAvailableArticles: VersionArticle[] = await this.versionRepository.find({where: {isDeleted: false, isCurrent: true}});
        return plainToClass(ShowArticleDTO, showAllAvailableArticles, {
            excludeExtraneousValues: true,
        });
    }

    async deleteArticleVersion(Id: string = 'null', artId: string = 'null'): Promise<any> {
        if (artId !== 'null') {
            const articlesToDelete: VersionArticle[] = await this.versionRepository.find({where: {article: artId}});
            Promise.all(articlesToDelete.map(async (article) =>  {
                article.isDeleted = true;
                await this.versionRepository.save(article);
            }));
        } else if (Id !== 'null') {
            const articleToDelete: any = await this.versionRepository.findOne({where: {id: Id}, relations: ['article']});
            articleToDelete.isDeleted = true;
            await this.versionRepository.save(articleToDelete);
            if (articleToDelete.isCurrent) {
                articleToDelete.isCurrent = false;
                const currentArticle: any = await this.versionRepository.findOne(
                    {where: {article: articleToDelete.__article__.id, isDeleted: false}, order: {
                    version: 'DESC',
                },  relations: ['user'] });
                if (currentArticle) {
                    currentArticle.isCurrent = true;
                    this.versionRepository.save(currentArticle);
                }
            }
        }
    }

    async detectLanguage(text: string) {
        const lang = this.translationService.detectLanguage(text);
        return lang;
    }

    async getArticleById(articleId: string, langCode: string): Promise<ShowArticleByIdDTO> {
        const article: VersionArticle = await this.versionRepository.findOne({where: {id: articleId, isCurrent: true}, relations: ['user']});
        const currentArticle = await this.translationService.getArticleByLang(article, langCode);
        return plainToClass(ShowArticleByIdDTO, currentArticle, {
            excludeExtraneousValues: true,
        });
    }

    async getArticle(articleId: string): Promise<ArticleUpdateDTO> {
        const article: VersionArticle = await this.versionRepository.findOne({where: {id: articleId, isCurrent: true}, relations: ['article']});
        return plainToClass(ArticleUpdateDTO, article, {
            excludeExtraneousValues: true,
        });
    }
}
