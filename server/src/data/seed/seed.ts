import { createConnection } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { UserRole } from '../enums/user-type';
import { User } from '../enitites/user.entity';
import { Role } from '../enitites/role.entity';
import { Language } from '../enitites/languages.entity';
import { TranslationUI } from '../enitites/translation-ui.entity';
// tslint:disable-next-line: no-var-requires
const {Translate} = require('@google-cloud/translate').v2;
const translate = new Translate();
const main = async () => {

  const connection = await createConnection();
  const userRepo = connection.getRepository(User);
  const roleRepo = connection.getRepository(Role);
  const langRepo = connection.getRepository(Language);
  const uiRepo = connection.getRepository(TranslationUI);

  const uiComponents = {
    SignIn: 'Sign in',
    Articles: 'Articles',
    Submit: 'Submit',
    ChooseFile: 'Choose File',
    Id: 'Id',
    Username: 'Username',
    Name: 'Name',
    Surname: 'Surname',
    Email: 'Email',
    Password: 'Password',
    Roles: 'Roles',
    Operations: 'Operations',
    RegisteredUsers: 'Registered Users',
    OriginalText: 'Original Text',
    TranslatedText: 'Translated Text',
    SaveChanges: 'Save Changes',
    Translations: 'Translations',
    Text: 'Text',
    TranslationLanguage: 'Translation Language',
    EditTranslation: 'Edit Translation',
    Register: 'Register',
    LogOut: 'Log out',
    AdminPanel: 'Admin Panel',
    MostRecentArticles: 'Most Recent Articles',
    ReadMore: 'Read More',
    CreatedBy: 'Created by',
    Technologies: 'Technologies',
    ThanksTo: 'Thanks to',
    EnterUsername: 'Enter username',
    EnterName: 'Enter name',
    EnterSurname: 'Enter surname',
    EnterEmail: 'Enter email',
    EnterPassword: 'Enter password',
    DefaultArticleImage: 'Default article image',
    ArticleContent: 'Article content',
    ArticleTitle: 'Article title',
    Create: 'Create',
    CreateNewArticle: 'Create new article',
    BanUser: 'Ban User',
    UnbanUser: 'Unban User',
    DeleteUser: 'Delete User',
    Search: 'Search',
    SucLogOut: 'Succesful logout!',
    InvalidUP: 'Invalid username or password!',
    SucLogIn: 'Succesful login!',
    SucReg: 'Succesful register!',
    InvalidC: 'Invalid credentials!',
    InvalidLogOut: 'Invalid log out!',
    ActiveRoles: 'Active Roles',
    UploadPicture: 'Upload Picture',
    EditProfile: 'Edit Profile',
    ProfileUpdate: 'Profile Update',
    Save: 'Save',
    Profile: 'Profile',
    ChooseLanguage: 'Choose language',
    AvgRate: 'Average Rate of Content Translation',
    LastUpdate: 'Last update',
    Title: 'Title',
    Content: 'Content',
    EnterTitle: 'Enter Title',
    EnterContent: 'Enter content',
    InputInfo: 'Title must be at least 10 characters !',
    ContentInfo: 'Content must be at least 50 characters !',
    LanguageConfirm: 'Language Confirm',
    LanguageConfirmQuestion: 'Are u sure that language is:',
    ShowVersions: 'Show details',
    SetVersion: 'Set version',
    GoBack: 'Back',
    ArticleSet: 'Current version set',
    ArticleSetFail: 'Unable to set version',
    ArticleDelete: 'Article deleted',
    ArticleDeleteFail: 'Unable to delet article',
    DeleteArticle: 'Delete article',
    UpdateArticle: 'Article updated',
    UpdateArticleFail: 'Unable to update article',
    EditArticle: 'Edit article',
    MyArticles: 'My Articles',
    CreatedArticle: 'Article was created!',
    ViewDetails: 'View Details',
    UpdateUser: 'Update User',
    SetEditorRole: 'Set Editor Role',
    CreateNewLanguage: 'Create New Language',
    ArticleVersionInfo: 'You have no current version. Please set version.',
    UsernameInfo: 'Minimum 6, Maximum 15 characters length.',
    NameInfo: 'Minimum 3, Maximum 15 characters length.',
    SurnameInfo: 'Minimum 3, Maximum 15 characters length.',
    EmailInfo: 'Maximum 30 characters length.',
    PasswordInfo: 'Minimum 5 characters length.',
    RateThanks: 'Thank you for the rate!',
    Language: 'Language',
    LanguageCode: 'LanguageCode',
    UnbanQuestion: 'Are u sure you want unban',
    BanQuestion: 'Are you sure you want to ban',
    DeleteQuestion: 'Are you sure you want to delete',
    ArticleUpdated: 'Translation was updated !',

  };

  const bg = await langRepo.save({
    language: 'Bulgarian',
    code: 'bg',
  });
  const en = await langRepo.save({
  language: 'English',
  code: 'en',
});
  const es = await langRepo.save({
  language: 'Spanish',
  code: 'es',
});
  const ar = await langRepo.save({
    language: 'Arabic',
    code: 'ar',
  });
  const it = await langRepo.save({
    language: 'Italian',
    code: 'it',
  });
  const admin = await roleRepo.save({
    role: UserRole.Admin,
  });
  const contributor = await roleRepo.save({
    role: UserRole.Contributor,
  });
  const editor = await roleRepo.save({
    role: UserRole.Editor,
  });

  const firstAdmin = new User();
  firstAdmin.username = 'John',
  firstAdmin.email = 'John@gmail.com';
  firstAdmin.name = 'John';
  firstAdmin.surname = 'Johnson';
  firstAdmin.password = await bcrypt.hash('John123', 10);
  firstAdmin.roles = ([admin, contributor, editor]);
  firstAdmin.createdOn = new Date().toLocaleString();

  await userRepo.save(firstAdmin);

  const allSupportedLanguages = await langRepo.find();

  for (const language in allSupportedLanguages) {
    const lang = allSupportedLanguages[language];
    // tslint:disable-next-line: forin
    for (const component in uiComponents) {
        const value = uiComponents[component];
        const translation = (await translate.translate(value, `${lang.code}`))[0];
        const uiTranslate = uiRepo.create();
        uiTranslate.text = value;
        uiTranslate.targetLanguage = lang.code;
        uiTranslate.translatedText = await translation;
        await uiRepo.save(uiTranslate);
  }

    }

  await connection.close();

  // tslint:disable-next-line: no-console
  console.log(`Data seeded successfully`);

};

main()
  // tslint:disable-next-line: no-console
  .catch(console.log);
