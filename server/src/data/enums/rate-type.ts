export enum TranslateRatingEnum {
    VeryPoor = 1,
    Poor = 2,
    Average = 3,
    Good = 4,
    VeryGood = 5,
}
