export enum UserRole {
    Admin = 'Admin',
    Contributor = 'Contributor',
    Editor = 'Editor',
}
