import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { TranslateRatingEnum } from '../enums/rate-type';
import { TranslationArticle } from './translation-article.entity';

@Entity('rating')
export class TranslationRating {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({type: 'enum', enum: TranslateRatingEnum, default: TranslateRatingEnum.VeryPoor})
    public rating: TranslateRatingEnum;

    @Column({type: 'nvarchar'})
    public cookie: string;

    @ManyToOne(type => TranslationArticle, translation => translation.rating)
    public translation: Promise<TranslationArticle>;
}
