import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Article } from './article.entity';

@Entity('article versions')
export class VersionArticle {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @ManyToOne(type => Article, article => article.version)
    public article: Promise<Article>;

    @Column({ nullable: true })
    public imgUrl: string;

    @Column('longtext')
    public content: string;

    @Column('longtext')
    public title: string;

    @Column({type: 'integer'})
    public version: number;

    @Column({type: 'boolean', default: false})
    public isCurrent: boolean;

    @Column({type: 'nvarchar'})
    public createdOn: string;

    @Column({type: 'nvarchar'})
    public updatedOn: string;

    @Column({type: 'boolean', default: false})
    public isDeleted: boolean;

    @ManyToOne(type => User, user => user.articles)
    public user: Promise<User>;
}
