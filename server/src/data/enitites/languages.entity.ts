import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('languages')
export class Language {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar')
    public language: string;

    @Column('nvarchar')
    public code: string;
}
