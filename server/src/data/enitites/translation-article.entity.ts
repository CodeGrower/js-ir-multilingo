import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { TranslationRating } from './rate.entity';

@Entity('translationArticle')
export class TranslationArticle {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('longtext', {default: null})
    public text: string;

    @Column('nvarchar', {default: null})
    public originalLanguage: string;

    @Column('nvarchar', {default: null})
    public targetLanguageCode: string;

    @Column('nvarchar', {default: null})
    public targetLanguage: string;

    @Column('longtext', {default: null})
    public translatedText: string;

    @Column({default: false})
    public isOriginal: boolean;

    @OneToMany(type => TranslationRating, rating => rating.translation)
    public rating: Promise<TranslationRating[]>;
}
