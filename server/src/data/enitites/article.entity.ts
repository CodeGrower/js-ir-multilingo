import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { VersionArticle } from './article-version.entity';

@Entity('articles')
export class Article {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @ManyToOne(type => User, user => user.articles)
    public user: Promise<User>;

    @OneToMany(type => VersionArticle, article => article.article)
    public version: Promise<any>;

    @Column({default: false})
    public isDeleted: boolean;

}
