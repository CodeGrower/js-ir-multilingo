import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('role')
export class Role {

    @PrimaryGeneratedColumn('increment')
    public id: string;

    @Column('nvarchar', {length: 15})
    @Column({ nullable: false })
    public role: string;
}
