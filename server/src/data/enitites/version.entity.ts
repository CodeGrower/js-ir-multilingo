import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('version')
export class Version {
    @PrimaryGeneratedColumn('uuid')
    public contentId: string;

    @Column('nvarchar')
    public text: string;

    @Column('integer')
    public version: number;

    @Column('nvarchar', {length: 20})
    public language: string;

    @Column('nvarchar')
    public translation: string;
}
