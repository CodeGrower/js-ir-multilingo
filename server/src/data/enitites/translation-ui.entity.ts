import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('translationUI')
export class TranslationUI {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('longtext', {default: null})
    public text: string;

    @Column('nvarchar', {default: null})
    public targetLanguage: string;

    @Column('longtext', {default: null})
    public translatedText: string;

}
