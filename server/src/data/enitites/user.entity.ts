import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { Role } from './role.entity';
import { Article } from './article.entity';

@Entity('users')
export class User {

    @PrimaryGeneratedColumn('increment')
    public id: string;

    @Column('nvarchar', {nullable: false, length: 15})
    public username: string;

    @Column('nvarchar', {nullable: false, length: 15})
    public name: string;

    @Column('nvarchar', {nullable: false, length: 15})
    public surname: string;

    @Column('nvarchar', {nullable: false, length: 30})
    public email: string;

    @Column({type: 'nvarchar', default: false})
    public createdOn: string;

    @Column('nvarchar')
    public password: string;

    @Column({default: false})
    public isDeleted: boolean;

    @Column({default: false})
    public isBanned: boolean;

    @ManyToMany(type => Role, { eager: true })
    @JoinTable()
    public roles: Role[];

    @Column({ nullable: true })
    public avatarUrl: string;

    @OneToMany(type => Article, article => article.user)
    public articles: Promise<Article[]>;

}
