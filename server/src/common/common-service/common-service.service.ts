import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Language } from '../../data/enitites/languages.entity';

@Injectable()
export class CommonServiceService {

    constructor(
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
    ) {}

    async getCurrentLanguage(language: string): Promise<string[]> {
        const lang = language.split('');
        const index = lang.indexOf('-');
        const check1 = (lang[0] === lang[0].toUpperCase());
        let langCodeCheck: string;
        let langCode;
        if (index < 0 && check1 === true) {
            langCodeCheck = language;
            const currentLanguage = await this.languageRepository.findOne({where: {language: langCodeCheck}});
            langCode = currentLanguage.code;
        } else if (index > 0) {
            langCodeCheck = lang.splice(0, index).join('');
            const currentLanguage = await this.languageRepository.findOne({where: {code: langCodeCheck}});
            langCode = currentLanguage.code;
        } else {
            langCodeCheck = language;
            const currentLanguage = await this.languageRepository.findOne({where: {code: langCodeCheck}});
            if (!currentLanguage) {
                langCode = 'en';
            } else {
                langCode = currentLanguage.code;
            }
        }
        return [langCodeCheck, langCode];
    }
}
