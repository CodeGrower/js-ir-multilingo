import { Catch, ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { GlobalSystemError } from '../exceptions/global-system.error';
import { Response } from 'express';

@Catch(GlobalSystemError)
export class GlobalSystemErrorFilter implements ExceptionFilter {
  public catch(exception: GlobalSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
