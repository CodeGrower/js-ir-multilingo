import { Module, Global } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { ConfigModule } from '../config/config.module';
import { CommonServiceService } from './common-service/common-service.service';
import { Language } from '../data/enitites/languages.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Global()
@Module({
  imports: [ConfigModule, AuthModule, TypeOrmModule.forFeature([Language])],
  exports: [ConfigModule, AuthModule, Language],
  providers: [CommonServiceService, Language],
})
export class CoreModule {}
