import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { UserRole } from '../../data/enums/user-type';

@Injectable()
export class AdminGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const check = user.roles.includes('Admin');
    return user && check;
  }
}
