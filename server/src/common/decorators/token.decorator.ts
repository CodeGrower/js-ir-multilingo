import { createParamDecorator } from '@nestjs/common';
import { Request } from 'express';
// tslint:disable-next-line:variable-name
export const Token = createParamDecorator((_, req: Request) => req.headers.authorization);
