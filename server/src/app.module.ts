import { Module } from '@nestjs/common';
import { UsersModule } from './users/user.module';
import { DatabaseModule } from './data/database.module';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './common/core.module';
import { ArticlesModule } from './articles/articles.module';
import { ArticleVersionModule } from './article-version/article-version.module';
import { TranslationModule } from './article-translation/translation.module';
import { UiComponentsModule } from './ui-translation/ui-components.module';

@Module({
  imports: [UsersModule, ArticlesModule, DatabaseModule, AuthModule, CoreModule, ArticleVersionModule, UiComponentsModule, TranslationModule],
  providers: [],
})
export class AppModule {}
