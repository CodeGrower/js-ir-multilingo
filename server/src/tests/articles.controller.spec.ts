import { Test, TestingModule } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { User } from '../data/enitites/user.entity';
import { ArticlesController } from '../articles/articles.controller';
import { ArticlesService } from '../articles/articles.service';
import { CreateArticleDTO } from '../articles/models/createArticleDTO';

describe('Articles Controller', () => {
    let controller: ArticlesController;
    const articlesService = {
        getAllArticles() {/* empty */ },
        addNewArticle() { /* empty */ },
        deleteArticle() { /* empty */ },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                PassportModule.register({defaultStrategy: 'jwt'}),
            ],
            controllers: [ArticlesController],
            providers: [
                {
                    provide: ArticlesService,
                    useValue: articlesService,
                },
            ],
        }).compile();

        controller = module.get<ArticlesController>(ArticlesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    //                               !===---getAllArticles---===!

    it('getAllArticles should call articlesService.getAllArticles', async () => {
        // Arrange
        const spy = jest.spyOn(articlesService, 'getAllArticles');
        const mockLanguage = 'en';
        const mockTitle = 'Life';

        // Act
        await controller.getAllArticles(mockTitle);

        // Assert
        expect(articlesService.getAllArticles).toHaveBeenCalledTimes(1);

        spy.mockClear();
    });

    it('getAllArticles should return the result from articlesService.getAllArticles', async () => {
        // Arrange
        const mockArticles = [{title: 'My life', content: 'Is all about music' },
        {title: 'My life', content: 'Is lovely pet' }];
        const mockLanguage = 'en';
        const mockTitle = 'Life';
        const spy = jest.spyOn(articlesService, 'getAllArticles').mockImplementation(async () => mockArticles);

        // Act
        const response = await controller.getAllArticles(mockTitle);

        // Assert
        expect(response).toBe(mockArticles);

        spy.mockClear();
    });

    //                               !===---addNewArticle---===!

    it('addNewArticle should call articlesService.addNewArticle with the correct arguments', async () => {
        // Arrange
        const article: CreateArticleDTO = {title: 'My life', content: 'Is all about music', origLang: 'en', imgUrl: ''};
        const user = new User();
        const spy = jest.spyOn(articlesService, 'addNewArticle');
        const origLang = 'en';
        // Act
        await controller.addNewArticle(article, user);

        // Assert
        expect(articlesService.addNewArticle).toHaveBeenCalledTimes(1);

        spy.mockClear();
    });

    it('addNewBook should return the result from bookService.addNewBook', async () => {
        // Arrange
        const article = {title: 'My life', content: 'Is all about music' };
        const user = new User();
        const articleToReturn = {title: 'My life', content: 'Is all about music'};
        const spy = jest.spyOn(articlesService, 'addNewArticle').mockImplementation(async () => article);

        // Act
        const response = await controller.addNewArticle(article, user);

        // Assert
        expect(response).toStrictEqual(articleToReturn);

        spy.mockClear();
    });

        //                               !===---deleteArticle---===!

    it('deleteArticle should call articlesService.deleteArticle with correct argument', async () => {
        // Arrange
        const id = '1';
        const spy = jest.spyOn(articlesService, 'deleteArticle');

        // Act
        await controller.deleteArticle(id);

        // Assert
        expect(articlesService.deleteArticle).toHaveBeenCalledTimes(1);
        expect(articlesService.deleteArticle).toHaveBeenCalledWith(id);

        spy.mockClear();
    });

    it('deleteArticle should return result from articlesService.deleteArticle', async () => {
        // Arrange
        const id = '1';
        const article = {title: 'My life', content: 'Is all about music' };
        const spy = jest.spyOn(articlesService, 'deleteArticle').mockImplementation(async () => article);

        // Act
        const response = await controller.deleteArticle(id);

        // Assert
        expect(response).toBe(article);

        spy.mockClear();
    });

});
