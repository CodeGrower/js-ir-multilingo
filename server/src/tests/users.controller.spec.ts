import { Test, TestingModule } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { UsersController } from '../users/users.controller';
import { UsersDataService } from '../users/user-data.service';
import { CreateUserDTO } from '../users/models/createUserDTO';
import { UserRole } from '../data/enums/user-type';
import { CreateLanguageDTO } from '../users/models/createLanguageDTO';

describe('Users Controller', () => {
  let controller: UsersController;
  const usersService = {
    createUser() { /* empty */ },
    banUser() { /* empty */ },
    unBanUser() { /*empty */ },
    deleteUser() { /* empty */ },
    updateUser() { /* empty */ },
    createEditor() { /* empty */ },
    addNewLanguage() { /* empty */ },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({defaultStrategy: 'jwt'}),
      ],
      controllers: [UsersController],
      providers: [
        {
          provide: UsersDataService,
          useValue: usersService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {

    expect(controller).toBeDefined();

  });

  it('controller.createUser should call usersService.createUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'createUser');
    const user: CreateUserDTO = {username: '123', name: 'Ivan', surname: 'Ivanov', email: 'IvanIvanov@gmail.com', password: '123'};
    // Act
    await controller.createUser(user);

    // Assert
    expect(usersService.createUser).toHaveBeenCalledTimes(1);

    spy.mockClear();

  });

  it('controller.createUser should return the result from usersService.createUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'createUser').mockImplementation(async () => 'test');
    const user: CreateUserDTO = {username: '123', name: 'Ivan', surname: 'Ivanov', email: 'IvanIvanov@gmail.com', password: '123'};
    // Act
    const response = await controller.createUser(user);

    // Assert
    expect(response).toBe('test');

    spy.mockClear();

  });

  it('create should call usersService.createUser with the correct argument', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'createUser');
    const user: CreateUserDTO = {username: '123', name: 'Ivan', surname: 'Ivanov', email: 'IvanIvanov@gmail.com', password: '123'};

    // Act
    await controller.createUser(user);

    // Assert
    expect(usersService.createUser).toHaveBeenCalledWith(user, UserRole.Contributor);

    spy.mockClear();

  });
  it('controller.banUser should call usersService.banUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'banUser');
    const userId: string = '123qwe';
    // Act
    await controller.banUser(userId);

    // Assert
    expect(usersService.banUser).toHaveBeenCalledTimes(1);

    spy.mockClear();

  });

  it('controller.banUser should return the result from usersService.banUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'banUser').mockImplementation(async () => 'test');
    const userId: string = '123qwe';

    // Act
    const response = await controller.banUser(userId);

    // Assert
    expect(response).toBe('test');

    spy.mockClear();

  });

  it('controller.banUser should call usersService.banUser with the correct argument', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'banUser');
    const userId: string = '123qwe';

    // Act
    await controller.banUser(userId);

    // Assert
    expect(usersService.banUser).toHaveBeenCalledWith(userId);

    spy.mockClear();
  });

  it('controller.unBanUser should call usersService.unBanUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'unBanUser');
    const userId: string = '123qwe';
    // Act
    await controller.unBanUser(userId);

    // Assert
    expect(usersService.unBanUser).toHaveBeenCalledTimes(1);

    spy.mockClear();

  });

  it('controller.unBanUser should return the result from usersService.unBanUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'unBanUser').mockImplementation(async () => 'test');
    const userId: string = '123qwe';

    // Act
    const response = await controller.unBanUser(userId);

    // Assert
    expect(response).toBe('test');

    spy.mockClear();

  });

  it('controller.unBanUser should call usersService.unBanUser with the correct argument', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'unBanUser');
    const userId: string = '123qwe';

    // Act
    await controller.unBanUser(userId);

    // Assert
    expect(usersService.unBanUser).toHaveBeenCalledWith(userId);

    spy.mockClear();
  });

  it('controller.deleteUser should call usersService.deleteUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'deleteUser');
    const userId: string = '123qwe';
    // Act
    await controller.deleteUser(userId);

    // Assert
    expect(usersService.deleteUser).toHaveBeenCalledTimes(1);

    spy.mockClear();

  });

  it('controller.deleteUser should return the result from usersService.deleteUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'deleteUser').mockImplementation(async () => 'test');
    const userId: string = '123qwe';

    // Act
    const response = await controller.deleteUser(userId);

    // Assert
    expect(response).toBe('test');

    spy.mockClear();

  });

  it('controller.deleteUser should call usersService.deleteUser with the correct argument', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'deleteUser');
    const userId: string = '123qwe';

    // Act
    await controller.deleteUser(userId);

    // Assert
    expect(usersService.deleteUser).toHaveBeenCalledWith(userId);

    spy.mockClear();
  });

  it('controller.uploadUserAvatar should call usersService.updateUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'updateUser');
    const userId: string = '123qwe';
    const mockFile = {avatarUrl: undefined};
    // Act
    await controller.uploadUserAvatar(userId, mockFile);

    // Assert
    expect(usersService.updateUser).toHaveBeenCalledTimes(1);

    spy.mockClear();

  });

  it('controller.uploadUserAvatar should return the result from usersService.updateUser', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'updateUser').mockImplementation(async () => 'test');
    const userId: string = '123qwe';
    const mockFile = {avatarUrl: undefined};
    // Act
    const response = await controller.uploadUserAvatar(userId, mockFile);

    // Assert
    expect(response).toBe('test');

    spy.mockClear();

  });

  it('controller.uploadUserAvatar should call usersService.updateUser with the correct argument', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'updateUser');
    const userId: string = '123qwe';
    const mockFile = {avatarUrl: undefined};
    // Act
    await controller.uploadUserAvatar(userId, mockFile);

    // Assert
    expect(usersService.updateUser).toHaveBeenCalledWith(userId, mockFile);

    spy.mockClear();
  });

  it('controller.addNewLanguage should call usersService.addNewLanguage', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'addNewLanguage');
    const newLanguage: CreateLanguageDTO = {language: 'Test', code: 'Test'};

    // Act
    await controller.addNewLanguage(newLanguage);

    // Assert
    expect(usersService.addNewLanguage).toHaveBeenCalledTimes(1);

    spy.mockClear();

  });

  it('controller.addNewLanguage should return the result from usersService.addNewLanguage', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'addNewLanguage').mockImplementation(async () => 'test');
    const newLanguage: CreateLanguageDTO = {language: 'Test', code: 'Test'};

    // Act
    const response = await controller.addNewLanguage(newLanguage);

    // Assert
    expect(response).toBe('test');

    spy.mockClear();

  });

  it('controller.addNewLanguage should call usersService.addNewLanguage with the correct argument', async () => {

    // Arrange
    const spy = jest.spyOn(usersService, 'addNewLanguage');
    const newLanguage: CreateLanguageDTO = {language: 'Test', code: 'Test'};

    // Act
    await controller.addNewLanguage(newLanguage);

    // Assert
    expect(usersService.addNewLanguage).toHaveBeenCalledWith(newLanguage);

    spy.mockClear();
  });

});
