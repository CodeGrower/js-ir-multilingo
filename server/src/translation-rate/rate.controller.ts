import { Controller, UseGuards, Put, Param, Body, ValidationPipe } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { RateDTO } from '../articles/models/RateDTO';
import { RateService } from './rate.service';

@Controller('rate')
// @UseGuards(AuthGuardWithBlacklisting)
export class RateController {
    constructor(
        private readonly rateService: RateService,
    ) {}
    @Put('/:language')
    // @UseGuards(AdminGuard)
    // tslint:disable-next-line:max-line-length
    async rateTranslate(@Body(new ValidationPipe({ whitelist: true, transform: true })) body: RateDTO, @Param('language') language: string): Promise<RateDTO> {
        return await this.rateService.rateTranslation(body.content, body, language);
    }
}
