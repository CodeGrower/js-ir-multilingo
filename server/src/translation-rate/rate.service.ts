import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TranslationRating } from '../data/enitites/rate.entity';
import { User } from '../data/enitites/user.entity';
import { RateDTO } from '../articles/models/RateDTO';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { plainToClass } from 'class-transformer';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Injectable()
export class RateService {
    constructor(
        @InjectRepository(TranslationArticle) private readonly translationRepository: Repository<TranslationArticle>,
        @InjectRepository(TranslationRating) private readonly rateRepository: Repository<TranslationRating>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        private readonly commonService: CommonServiceService,
    ) {}

    async rateTranslation(translation: string, body: RateDTO, language: string): Promise<RateDTO> {
        const langCode = await this.commonService.getCurrentLanguage(language);
        const foundTranslation: TranslationArticle = await this.translationRepository.findOne(
            {where: {translatedText: translation, targetLanguageCode: langCode[1]}});
        if (!foundTranslation) {
            throw new GlobalSystemError(`That translation doesn't exist !`, 400);
        }
        const rating: TranslationRating = this.rateRepository.create(body);
        rating.cookie = body.cookie;
        rating.translation = Promise.resolve(foundTranslation);
        const rate = await this.rateRepository.findOne({where: {cookie: body.cookie, translation: foundTranslation.id}});
        let savedRating;
        if (rate) {
            const newRate = {...rate, ...rating};
            savedRating = await this.rateRepository.save(newRate);
        } else {
            savedRating = await this.rateRepository.save(rating);
        }
        return plainToClass(RateDTO, savedRating, {
            excludeExtraneousValues: true,
        });
    }

    async resetRate(artId: string): Promise<TranslationRating[]> {
        const translationEdited = await this.rateRepository.find({where: {translation: artId}});
        await this.rateRepository.remove(translationEdited);
        return translationEdited;
    }

    async getRateCookies(): Promise<TranslationRating[]> {
        const test = await this.rateRepository.find({relations: ['translation']});
        return test;
    }
}
