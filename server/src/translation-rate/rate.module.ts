import { Module } from '@nestjs/common';
import { RateService } from '../translation-rate/rate.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TranslationRating } from '../data/enitites/rate.entity';
import { User } from '../data/enitites/user.entity';
import { RateController } from './rate.controller';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { CommonServiceService } from '../common/common-service/common-service.service';
import { Language } from '../data/enitites/languages.entity';

@Module({imports: [TypeOrmModule.forFeature([TranslationArticle, TranslationRating, User, Language])],
    providers: [RateService, CommonServiceService],
    controllers: [RateController],
    exports: [RateService],
})
export class RateModule {}
