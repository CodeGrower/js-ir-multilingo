import { AuthService } from './auth.service';
import { Controller, Get, Delete, Body } from '@nestjs/common';
import { Post, UseGuards } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { Token } from '../common/decorators/token.decorator';
import { LoginDTO } from './login.dto';

@Controller('session')
export class AuthController {

    constructor(private readonly authService: AuthService) {}

    @Get('/ipAddress')
    public async getIpAddress() {
      const ipAddress = {ipAddress: ''};
      const ip = await this.authService.getIpAddress();
      ipAddress.ipAddress = ip;
      return ipAddress;
    }

    @Post()
    public async login(@Body() body: LoginDTO) {
    return this.authService.login(body);
    }
    @Delete()
    // @UseGuards(AuthGuardWithBlacklisting)
    public async logout(@Token() token: string) {
      this.authService.blacklistToken(token);

      return {
        msg: 'Successful logout!',
      };
    }
}
