import { JwtModule } from '@nestjs/jwt';
import { Module, HttpService, HttpModule } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt.strategy';
import { AuthController } from './auth.controller';
import { ConfigService } from '../config/config.service';
import { ConfigModule } from '../config/config.module';
import { jwtConstants } from './constants';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersDataService } from './../users/user-data.service';
import { User } from '../data/enitites/user.entity';
import { Role } from '../data/enitites/role.entity';
import { UsersModule } from '../users/user.module';
import { ArticlesModule } from '../articles/articles.module';
import { Language } from '../data/enitites/languages.entity';
import { TranslationUiComponentsService } from '../ui-translation/ui-components.service';
import { TranslationService } from '../article-translation/translation.service';
import { TranslationUI } from '../data/enitites/translation-ui.entity';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { RateModule } from '../translation-rate/rate.module';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Module({

  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  TypeOrmModule.forFeature([User, Role, Language, TranslationUI, TranslationArticle]),
    UsersModule,
    ConfigModule,
    RateModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: jwtConstants.secret,
        signOptions: {
          expiresIn: jwtConstants.expiresIn,
        },
      }),
    }),
  ArticlesModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, UsersDataService, TranslationUiComponentsService, TranslationService, CommonServiceService],
  exports: [AuthService],

})
export class AuthModule {}
