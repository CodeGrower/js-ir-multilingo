import { Injectable, BadRequestException, HttpService } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersDataService } from './../users/user-data.service';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {
    private readonly blacklist: string[] = [];

    constructor(
      private readonly usersService: UsersDataService,
      private readonly jwtService: JwtService,
      private readonly http: HttpService,
      ) {}

    public async getIpAddress() {
      const value = await this.http.get<{ip: string}>('https://jsonip.com').toPromise();
      if (value) {
        return value.data.ip;
      }
      return;
    }
    public async login(user: any) {
        const foundUser = await this.usersService.findUserByUsername(user.username);
        if (!foundUser) {
          throw new BadRequestException(
            'User with such username does not exist!',
          );
        }
        if (!(await this.usersService.validateUserPassword(user))) {
          throw new GlobalSystemError('Invalid password!', 400);
        }
        const payload = { ...foundUser };
        return {
              access_token: await this.jwtService.signAsync(payload),
          };
      }

    public blacklistToken(token: string): void {
        this.blacklist.push(token);
    }

    public isTokenBlacklisted(token: string): boolean {
        return this.blacklist.includes(token);
    }
}
