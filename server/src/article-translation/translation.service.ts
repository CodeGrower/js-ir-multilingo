import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';
import { Language } from '../data/enitites/languages.entity';
import { VersionArticle } from '../data/enitites/article-version.entity';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { UpdateTranslationDTO } from '../articles/models/updateTranslationDTO';
import { plainToClass } from 'class-transformer';
import { RateService } from '../translation-rate/rate.service';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { TranslationRating } from '../data/enitites/rate.entity';
import { CommonServiceService } from '../common/common-service/common-service.service';

// tslint:disable-next-line:no-var-requires
const {Translate} = require('@google-cloud/translate').v2;
const translate = new Translate();
@Injectable()
export class TranslationService {
    constructor(
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        @InjectRepository(TranslationArticle) private readonly translationRepository: Repository<TranslationArticle>,
        private readonly rateService: RateService,
        private readonly commonService: CommonServiceService,
    ) {}
        async getTranslationByText(content: string) {
            return await this.translationRepository.findOne({where: {text: content, isOriginal: true}});
        }
    async translateText(text: string, language: string) {
        const languageResult = await this.commonService.getCurrentLanguage(language);
        const suppLanguages = await this.languageRepository.find();
        const detectLanguage = await translate.detect(text);
        const textLanguage = detectLanguage[0].language;
        for (const el of suppLanguages) {
            const englishText = (await translate.translate(text, `en`))[0];
            // tslint:disable-next-line:max-line-length
            const check = await this.translationRepository.findOne({where: {text: englishText, targetLanguageCode: el.code, originalLanguage: languageResult[1]}});
            if (check === undefined) {
            const origTranslate = this.translationRepository.create();
            if (el.code === textLanguage) {
                origTranslate.isOriginal = true;
            }
            origTranslate.rating = Promise.resolve([]);
            origTranslate.targetLanguageCode = el.code;
            origTranslate.targetLanguage = el.language;
            origTranslate.originalLanguage = languageResult[1];
            origTranslate.text = (await translate.translate(text, `en`))[0];
            origTranslate.translatedText = (await translate.translate(text, `${el.code}`))[0];
            await this.translationRepository.save(origTranslate);
            }
        }
    }
    async getTranslationById(translationId: string): Promise<TranslationArticle> {
        return await this.translationRepository.findOne({where: {id: translationId}});
    }
    async getArticleByLang(currentArticle: any, language: string): Promise<VersionArticle> {
        const languageResult = await this.commonService.getCurrentLanguage(language);
        const requiredText = (await translate.translate(currentArticle.content, `en`))[0];
        const findRateByText =  await this.translationRepository.findOne(
            {where : {text: requiredText, targetLanguageCode: languageResult[1]}, relations: ['rating']});
        // tslint:disable-next-line:prefer-const
        let { avg, people } = await getRepository(TranslationRating)
        .createQueryBuilder('rating')
        .select('AVG(rating)', 'avg')
        .addSelect('COUNT(rating)', 'people')
        .where('translationId = :translationId', { translationId: findRateByText.id })
        .getRawOne();
        const rateCookies: any = [];
        if (findRateByText.isOriginal === false) {
            const allRates: any = await this.rateService.getRateCookies();
            allRates.forEach((el: { __translation__: { id: string; }; cookie: string; rating: number }) => {
                if (el.__translation__.id === findRateByText.id) {
                    rateCookies.push({
                        cookie: el.cookie,
                        rate: el.rating,
                    });
                }
            });
        } else {
            avg = 0;
        }

        const check = await this.languageRepository.findOne({where: {code: languageResult[1]}});
        if (check !== undefined) {
            const content = (await translate.translate(currentArticle.content, `${languageResult[1]}`))[0];
            const title = (await translate.translate(currentArticle.title, `${languageResult[1]}`))[0];
            currentArticle.content = content;
            currentArticle.title = title;
            currentArticle.rating = avg;
            currentArticle.rateCookies = rateCookies;
            return currentArticle;
            } else {
                const content = (await translate.translate(currentArticle.content, `en`))[0];
                const title = (await translate.translate(currentArticle.title, `en`))[0];
                currentArticle.content = content;
                currentArticle.title = title;
                currentArticle.rating = avg;
                currentArticle.rateCookies = rateCookies;
                return currentArticle;
            }
    }
    async getAllArticlesByLang(currentArticles: VersionArticle[], language: string): Promise<VersionArticle[]> {
        const languageResult = await this.commonService.getCurrentLanguage(language);
        const showArticles: VersionArticle[] = await Promise.all(currentArticles.map(async el => {
            const engContent = (await translate.translate(el.content, `en`))[0];
            const engTitle = (await translate.translate(el.title, `en`))[0];
            const checkForContent = await this.translationRepository.findOne(
                {where : {text: engContent, targetLanguageCode: languageResult[1]}});
            const checkForTitle = await this.translationRepository.findOne(
                {where : {text: engTitle, targetLanguageCode: languageResult[1]}});
            if (checkForContent && checkForTitle) {
                el.content = checkForContent.translatedText;
                el.title = checkForTitle.translatedText;
                return el;
            } else {
                el.content = engContent;
                el.title = engTitle;
                return el;
        }
        }));
        return showArticles;
    }

    async editTranslatedArticle(artId: string, body: UpdateTranslationDTO): Promise<UpdateTranslationDTO> {
        const translationToEdit: TranslationArticle = await this.translationRepository.findOne({where: {id: artId, isOriginal: false}});
        if (!translationToEdit) {
            throw new GlobalSystemError('You can not edit original article!', 402);
        }
        const editedTranslation: TranslationArticle = {...translationToEdit, ...body};
        editedTranslation.rating = this.rateService.resetRate(artId);
        const translationToSave: TranslationArticle = await this.translationRepository.save(editedTranslation);

        return plainToClass(UpdateTranslationDTO, translationToSave, {
            excludeExtraneousValues: true,
        });
    }
            async detectLanguage(text: string): Promise<string> {
                const lang = await translate.detect(text);
                return lang;
            }

    async translateByNewLang(language: string, langCode: string) {
        const all = await this.translationRepository.find({where: {targetLanguageCode: 'en'}});
        const check = await this.languageRepository.findOne({where: {code: langCode}});
        if (check === undefined) {
        for (const el of all) {
            const newTranslate = this.translationRepository.create();
            newTranslate.targetLanguage = language;
            newTranslate.targetLanguageCode = langCode;
            newTranslate.text = el.text;
            newTranslate.originalLanguage = el.originalLanguage;
            newTranslate.translatedText = (await translate.translate(el.text, `${langCode}`))[0];
            await this.translationRepository.save(newTranslate);
        }
    } else {
        throw new GlobalSystemError('Language already exist!');
        }
    }

    async getAllTranslations(): Promise<TranslationArticle[]> {
        return await this.translationRepository.find({where: {isOriginal: false}});
    }
}
