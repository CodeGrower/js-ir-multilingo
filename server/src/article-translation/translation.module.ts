import { TranslationService } from './translation.service';
import { TranslationController } from './translation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Language } from '../data/enitites/languages.entity';
import { RateModule } from '../translation-rate/rate.module';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Module({
    imports: [TypeOrmModule.forFeature([Language, TranslationArticle]), RateModule],
    providers: [TranslationService, CommonServiceService],
    controllers: [TranslationController],
    exports: [TranslationService],
  })
  export class TranslationModule {}
