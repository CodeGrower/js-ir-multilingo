import { Controller, Put, Param, Body, ValidationPipe, UseGuards, Get } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { UpdateTranslationDTO } from '../articles/models/updateTranslationDTO';
import { TranslationService } from './translation.service';
import { TranslationArticle } from '../data/enitites/translation-article.entity';

@Controller('translations')
@UseGuards(AuthGuardWithBlacklisting)
export class TranslationController {
    constructor(
        private readonly translationService: TranslationService,
        ) {}

    @Get()
    async getAllTransations(): Promise<TranslationArticle[]> {
        return await this.translationService.getAllTranslations();
    }

    @Put(':translationId/edit')
    async editTranslatedArticle(@Param('translationId') translationId: string, @Body(new ValidationPipe({
         whitelist: true, transform: true })) body: UpdateTranslationDTO): Promise<UpdateTranslationDTO> {
        return await this.translationService.editTranslatedArticle(translationId, body);
    }

    @Get(':translationId')
    async getTranslationById(@Param('translationId') translationId: string) {
        return await this.translationService.getTranslationById(translationId);
    }
}
