import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';
import { GlobalSystemErrorFilter } from './common/filters/global-error.filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname, '..', 'avatars'), { prefix: '/avatars' });

  const options = new DocumentBuilder()
  .setTitle('MultiLingo API')
  .setDescription('MultiLingo')
  .setVersion('1.0')
  .addTag('MultiLingo')
  .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.useGlobalFilters(new GlobalSystemErrorFilter());
  app.enableCors();
  await app.listen(app.get(ConfigService).port);
}
bootstrap();
