import { Expose, Transform } from 'class-transformer';

export class ShowUserDTO {
  @Expose()
  public id: string;

  @Expose()
  public username: string;

  @Expose()
  public name: string;

  @Expose()
  public surname: string;

  @Expose()
  public email: string;

  @Expose()
  public avatarUrl: string;

  @Expose()
  public createdOn: string;

  @Expose()
  public isBanned: boolean;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.role))
  public roles: string[];

  @Expose()
  @Transform((_, obj) => obj.__articles__.map((x: any) => x.article))
  public ownArticles: string[];
}
