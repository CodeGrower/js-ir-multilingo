import { Length, Matches } from 'class-validator';

export class CreateUserDTO {
   @Length(2, 20)
   public username: string;

   @Length(2, 20)
   public name: string;

   @Length(2, 20)
   public surname: string;

   @Length(2, 20)
   public email: string;
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
   message:
     'The password must be minimum five characters, at least one letter and one number',
 })
   public password: string;
}
