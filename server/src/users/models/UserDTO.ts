import { UserRole } from 'src/data/enums/user-type';
import { Matches, Length, IsArray, IsNumberString, IsNotEmpty } from 'class-validator';

export class UserDTO {

    @IsNumberString()
    public id: string;

    @Length(2, 20)
    @IsNotEmpty()
    public username: string;

  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
    message:
      'The password must be minimum five characters, at least one letter and one number',
  })
  @IsNotEmpty()
    public password: string;

    @IsArray()
    public role: UserRole[];
}
