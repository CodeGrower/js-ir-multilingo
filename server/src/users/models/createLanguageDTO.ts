import { IsString } from 'class-validator';

export class CreateLanguageDTO {
    @IsString()
    public language: string;
    @IsString()
    public code: string;
}
