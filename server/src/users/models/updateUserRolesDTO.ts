import { IsArray } from 'class-validator';
import { UserRole } from '../../data/enums/user-type';

export class UpdateUserRolesDTO {
    @IsArray()
    public roles: UserRole[];
  }
