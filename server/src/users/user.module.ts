import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersDataService } from './user-data.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { diskStorage } from 'multer';
import { User } from '../data/enitites/user.entity';
import { Role } from '../data/enitites/role.entity';
import { MulterModule } from '@nestjs/platform-express';
import { extname } from 'path';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { Article } from '../data/enitites/article.entity';
import { ArticlesModule } from '../articles/articles.module';
import { Language } from '../data/enitites/languages.entity';
import { TranslationUiComponentsService } from '../ui-translation/ui-components.service';
import { TranslationService } from '../article-translation/translation.service';
import { TranslationUI } from '../data/enitites/translation-ui.entity';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { RateModule } from '../translation-rate/rate.module';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Module({
    imports: [TypeOrmModule.forFeature([User, Role, Article, Language, TranslationUI, TranslationArticle]), RateModule,
    MulterModule.register({
        fileFilter(_, file, cb) {
          const ext = extname(file.originalname);
          const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];
          if (!allowedExtensions.includes(ext)) {
            return cb(
              new GlobalSystemError('Only images are allowed', 400),
              false,
            );
          }
          cb(null, true);
        },
        storage: diskStorage({
          destination: './avatars',
          filename: (_, file, cb) => {
            const randomName = Array.from({ length: 32 })
              .map(() => Math.round(Math.random() * 10))
              .join('');
            return cb(null, `${randomName}${extname(file.originalname)}`);
          },
        }),
      }), ArticlesModule],
    controllers: [UsersController],
    providers: [UsersDataService, TranslationUiComponentsService, TranslationService, CommonServiceService],
})

export class UsersModule {}
