import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { User } from '../data/enitites/user.entity';
import { Role } from '../data/enitites/role.entity';
import { CreateUserDTO } from './models/createUserDTO';
import { UserRole } from 'src/data/enums/user-type';
import * as bcrypt from 'bcryptjs';
import { ShowUserDTO } from './models/showUserDTO';
import { plainToClass } from 'class-transformer';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { UpdateUserDTO } from './models/updateUserDTO';
import { ArticlesService } from '../articles/articles.service';
import { CreateLanguageDTO } from './models/createLanguageDTO';
import { Language } from '../data/enitites/languages.entity';
import { TranslationService } from '../article-translation/translation.service';
import { TranslationUiComponentsService } from '../ui-translation/ui-components.service';

@Injectable()
export class UsersDataService {
    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        private readonly articleService: ArticlesService,
        private readonly translationArticlesService: TranslationService,
        private readonly translationUiComponentsService: TranslationUiComponentsService,
    ) {}
    public async getUserById(userId: string): Promise<ShowUserDTO> {
      const foundUser: any = await this.usersRepository.findOne({where : {
        id: userId,
        isDeleted: false,
        isBanned: false,
      },
      relations: ['articles'],
      });
      if (foundUser === undefined) {
        throw new GlobalSystemError('No such user found', 404);
      }
      if (foundUser) {
        foundUser.article = foundUser.__articles__.map(x => x.article);
      }
      return plainToClass(ShowUserDTO, foundUser, {
        excludeExtraneousValues: true,
      });
    }

    public async validateUserPassword(user: CreateUserDTO): Promise<boolean> {
        const userEntity: User = await this.usersRepository.findOne({
          username: user.username,
        });
        return await bcrypt.compare(user.password, userEntity.password);
      }

    public async getAllUsers(): Promise<ShowUserDTO> {
        const userFounded: any = await this.usersRepository.find({where: {isDeleted: false}, relations: ['roles', 'articles']});
        return plainToClass(ShowUserDTO, userFounded, {
          excludeExtraneousValues: true,
        });
      }

    public async createUser(user: CreateUserDTO, ...roles: UserRole[]): Promise<ShowUserDTO> {
        const createdRoles: Role[] = await this.roleRepository.find({
          where: {
            role: In(roles),
          },
      });
        const foundUser: User = await this.usersRepository.findOne({where: { username: user.username, isDeleted: false }});
        const foundUser1: User = await this.usersRepository.findOne({where: { email: user.email, isDeleted: false }});
        if (foundUser || foundUser1) {
        throw new GlobalSystemError('User with that username or email already exist!', 400);
      }
        const createdUser: User = this.usersRepository.create(user);
        createdUser.password = await bcrypt.hash(user.password, 10);
        createdUser.roles = createdRoles;
        createdUser.articles = Promise.resolve([]);
        createdUser.createdOn = new Date().toLocaleDateString();
        const newUser: User = await this.usersRepository.save(createdUser);
        return plainToClass(ShowUserDTO, newUser, {
        excludeExtraneousValues: true,
      });
    }

    public async createEditor(userId: string, ...roles: UserRole[]): Promise<ShowUserDTO> {
      const createdRoles: Role[] = await this.roleRepository.find({
        where: {
          role: In(roles),
        },
    });
      const foundUser: User = await this.usersRepository.findOne({where: { id: userId }, relations: ['roles', 'articles']});
      if (!foundUser) {
      throw new GlobalSystemError('User with that email already exist!', 400);
      }
      if (!foundUser.roles.find((x) => x.id === createdRoles[0].id)) {
        foundUser.roles.push(createdRoles[0]);
      }
      const newUser: User = await this.usersRepository.save(foundUser);
      return plainToClass(ShowUserDTO, newUser, {
        excludeExtraneousValues: true,
      });
    }

public async findUserByUsername(username: string): Promise<ShowUserDTO> {
    const foundUser: any = await this.usersRepository.findOne({where : {
      username,
      isDeleted: false,
      isBanned: false,
    },
    relations: ['articles'],
    });
    if (foundUser === undefined) {
      throw new GlobalSystemError('No such user found', 404);
    }
    if (foundUser) {
      foundUser.article = foundUser.__articles__.map(x => x.article);
    }
    return plainToClass(ShowUserDTO, foundUser, {
      excludeExtraneousValues: true,
    });
  }

      async banUser(userId: string) {
          const foundUser: User = await this.usersRepository.findOne({id: userId});
          if (foundUser) {
              if (foundUser.isBanned === false) {
                  foundUser.isBanned = true;
                  await this.usersRepository.save(foundUser);
              }
          } else {
            throw new GlobalSystemError('User with that id does not exist!');
          }
      }
      async unBanUser(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({id: userId});
        if (foundUser) {
            if (foundUser.isBanned === true) {
                foundUser.isBanned = false;
                await this.usersRepository.save(foundUser);
            }
        } else {
          throw new GlobalSystemError('User with that id does not exist!');
        }
    }

      async deleteUser(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({id: userId});
        if (foundUser) {
          await this.articleService.deleteArticle(null, userId);
          if (foundUser.isDeleted === false) {
            foundUser.isDeleted = true;
            await this.usersRepository.save(foundUser);
              }
            } else {
              throw new GlobalSystemError('User with that id does not exist!');
            }
      }
      public async updateUser(id: string, user: Partial<UpdateUserDTO>,
      ): Promise<ShowUserDTO> {
        const foundUser: User = await this.usersRepository.findOne({where: { id }, relations: ['articles']});
        if (foundUser === undefined || foundUser.isDeleted) {
          throw new GlobalSystemError('No such user found', 404);
        }
        const entityToUpdate: User = { ...foundUser, ...user };
        const savedUser: User = await this.usersRepository.save(entityToUpdate);
        return plainToClass(ShowUserDTO, savedUser, {
          excludeExtraneousValues: true,
        });
      }

      public async addNewLanguage(body: CreateLanguageDTO) {
        const language = this.languageRepository.create(body);
        await this.translationArticlesService.translateByNewLang(language.language, language.code);
        await this.translationUiComponentsService.translateByNewLang(language.code);
        await this.languageRepository.save(language);
        return await this.languageRepository.find();
      }
}
