import { Controller, Post, Get, Body, Param, ValidationPipe, Put,
     UseGuards, HttpCode, HttpStatus, UseInterceptors, UploadedFile } from '@nestjs/common';
import { UsersDataService } from './user-data.service';
import { CreateUserDTO } from './models/createUserDTO';
import { ShowUserDTO } from './models/showUserDTO';
import { UserRole } from './../data/enums/user-type';
import { FileInterceptor } from '@nestjs/platform-express';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthGuardWithBlacklisting} from '../common/guards/blacklist.guard';
import { UpdateUserDTO } from './models/updateUserDTO';
import { CreateLanguageDTO } from './models/createLanguageDTO';

@Controller('users')
export class UsersController {

    constructor(private readonly service: UsersDataService) {}

    @Get()
    async getAllUsers(): Promise<ShowUserDTO> {
        return await this.service.getAllUsers();
    }

    @Post()
    async createUser(@Body(new ValidationPipe({ whitelist: true, transform: true })) body: CreateUserDTO): Promise<ShowUserDTO> {
        return await this.service.createUser(body, UserRole.Contributor);
    }

    @Put('/:userId/banned')
    @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
    async banUser(@Param('userId') userId: string) {
        return await this.service.banUser(userId);
    }

    @Put('/:userId/unbanned')
    @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
    async unBanUser(@Param('userId') userId: string) {
        return await this.service.unBanUser(userId);
    }

    @Put('/:userId')
    // @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
    async deleteUser(@Param('userId') userId: string) {
        return await this.service.deleteUser(userId);
    }

    @Post('/:id/avatar')
    @HttpCode(HttpStatus.CREATED)
    @UseInterceptors(FileInterceptor('file'))
    @UseGuards(AuthGuardWithBlacklisting)
    public async uploadUserAvatar(
        @Param('id') id: string,
        @UploadedFile() file: any,
    ): Promise<ShowUserDTO> {
        const updateUserProperties: Partial<UpdateUserDTO> = {
        avatarUrl: file.filename,
    };
        return await this.service.updateUser(id, updateUserProperties);
  }

  @Post('/:userId/editor')
  public async createEditor(@Param('userId') userId: string): Promise<ShowUserDTO> {
      return await this.service.createEditor(userId, UserRole.Editor);
  }

  @Post('/language')
  public async addNewLanguage(@Body(new ValidationPipe({ whitelist: true, transform: true })) body: CreateLanguageDTO) {
      return await this.service.addNewLanguage(body);
  }

  @Post('/:id/update')
  public async updateUser(@Param('id') id: string, @Body() updatedUser: Partial<UpdateUserDTO>) {
    return await this.service.updateUser(id, updatedUser);
  }

  @Get('/:userId')
  public async getUserById(@Param('userId') userId: string): Promise<ShowUserDTO> {
      return await this.service.getUserById(userId);
  }

}
