import { TranslationUI } from '../data/enitites/translation-ui.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Language } from '../data/enitites/languages.entity';
import { TranslationUiComponentsService } from './ui-components.service';
import { TranslationUiComponentsController } from './ui-components.controller';
import { Module } from '@nestjs/common';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Module({
    imports: [TypeOrmModule.forFeature([Language, TranslationUI])],
    providers: [TranslationUiComponentsService, CommonServiceService],
    controllers: [TranslationUiComponentsController],
  })
  export class UiComponentsModule {}
