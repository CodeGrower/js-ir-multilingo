import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Language } from '../data/enitites/languages.entity';
import { Repository } from 'typeorm';
import { TranslationUI } from '../data/enitites/translation-ui.entity';
import { CommonServiceService } from '../common/common-service/common-service.service';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
// tslint:disable-next-line:no-var-requires
const {Translate} = require('@google-cloud/translate').v2;
const translate = new Translate();
const components = {
    SignIn: 'Sign in',
    Submit: 'Submit',
    ChooseFile: 'Choose File',
    Id: 'Id',
    Username: 'Username',
    Name: 'Name',
    Surname: 'Surname',
    Email: 'Email',
    Password: 'Password',
    Roles: 'Roles',
    Operations: 'Operations',
    RegisteredUsers: 'Registered Users',
    OriginalText: 'Original Text',
    TranslatedText: 'Translated Text',
    SaveChanges: 'Save Changes',
    Translations: 'Translations',
    Text: 'Text',
    TranslationLanguage: 'Translation Language',
    EditTranslation: 'Edit Translation',
    Articles: 'Articles',
    Register: 'Register',
    LogOut: 'Log out',
    AdminPanel: 'Admin Panel',
    MostRecentArticles: 'Most Recent Articles',
    ReadMore: 'Read More',
    CreatedBy: 'Created by',
    Technologies: 'Technologies',
    ThanksTo: 'Thanks to',
    EnterUsername: 'Enter username',
    EnterName: 'Enter name',
    EnterSurname: 'Enter surname',
    EnterEmail: 'Enter email',
    EnterPassword: 'Enter password',
    DefaultArticleImage: 'Default article image',
    ArticleContent: 'Article content',
    ArticleTitle: 'Article title',
    Create: 'Create',
    CreateNewArticle: 'Create new article',
    BanUser: 'Ban User',
    UnbanUser: 'Unban User',
    DeleteUser: 'Delete User',
    Search: 'Search',
    SucLogOut: 'Succesful logout!',
    InvalidUP: 'Invalid username or password!',
    SucLogIn: 'Succesful login!',
    SucReg: 'Succesful register!',
    InvalidC: 'Invalid credentials!',
    InvalidLogOut: 'Invalid log out!',
    ActiveRoles: 'Active Roles',
    UploadPicture: 'Upload Picture',
    EditProfile: 'Edit Profile',
    ProfileUpdate: 'Profile Update',
    Save: 'Save',
    Profile: 'Profile',
    ChooseLanguage: 'Choose language',
    AvgRate: 'Average Rate of Content Translation',
    LastUpdate: 'Last update',
    Title: 'Title',
    Content: 'Content',
    EnterTitle: 'Enter citle',
    EnterContent: 'Enter content',
    InputInfo: 'Title must be at least 10 characters !',
    LanguageConfirm: 'Language Confirm',
    LanguageConfirmQuestion: 'Are u sure that language is:',
    ContentInfo: 'Content must be at least 50 characters !',
    ShowVersions: 'Show details',
    SetVersion: 'Set version',
    GoBack: 'Back',
    ArticleSet: 'Current version set',
    ArticleSetFail: 'Unable to set version',
    ArticleDelete: 'Article deleted',
    ArticleDeleteFail: 'Unable to delet article',
    DeleteArticle: 'Delete article',
    UpdateArticle: 'Article updated',
    UpdateArticleFail: 'Unable to update article',
    EditArticle: 'Edit article',
    MyArticles: 'My Articles',
    CreatedArticle: 'Article was created!',
    ViewDetails: 'View Details',
    UpdateUser: 'Update User',
    SetEditorRole: 'Set Editor Role',
    CreateNewLanguage: 'Create New Language',
    ArticleVersionInfo: 'You have no current version. Please set version.',
    UsernameInfo: 'Minimum 6, Maximum 15 characters length.',
    NameInfo: 'Minimum 3, Maximum 15 characters length.',
    SurnameInfo: 'Minimum 3, Maximum 15 characters length.',
    EmailInfo: 'Maximum 30 characters length.',
    PasswordInfo: 'Minimum 5 characters length.',
    RateThanks: 'Thank you for the rate!',
    Language: 'Language',
    LanguageCode: 'LanguageCode',
    UnbanQuestion: 'Are u sure you want unban',
    BanQuestion: 'Are you sure you want to ban',
    DeleteQuestion: 'Are you sure you want to delete',
    ArticleUpdated: 'Translation was updated !',
};

@Injectable()
export class TranslationUiComponentsService {
    constructor(
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        @InjectRepository(TranslationUI) private readonly translationUiRepository: Repository<TranslationUI>,
        private readonly commonService: CommonServiceService,
        ) {}

        async getComponentsByLang(text: string, language: string) {
            const languageResult = await this.commonService.getCurrentLanguage(language);
            if (languageResult[0] !== undefined) {
                const suppLanguages = await this.languageRepository.find();
                for (const el of suppLanguages) {
                    const check = await this.translationUiRepository.findOne({where: {text, targetLanguage: el.code}});
                    if (check === undefined) {
                        const origTranslate = this.translationUiRepository.create();
                        origTranslate.targetLanguage = el.code;
                        origTranslate.text = text;
                        origTranslate.translatedText = (await translate.translate(text, `${el.code}`))[0];
                        await this.translationUiRepository.save(origTranslate);
                    }
                }
                const res = await this.translationUiRepository.findOne({where: {text, targetLanguage: languageResult[1]}});
                return res.translatedText;
            }
            return this.getComponentsByLang(text, 'en');
        }

        async translateByNewLang(langCode: string) {
            const all = await this.translationUiRepository.find({targetLanguage: 'en'});
            const check = await this.languageRepository.findOne({where: {code: langCode}});
            if (check === undefined) {
            for (const el of all) {
                const newTranslate = this.translationUiRepository.create();
                newTranslate.targetLanguage = langCode;
                newTranslate.text = el.text;
                newTranslate.translatedText = (await translate.translate(el.text, `${langCode}`))[0];
                await this.translationUiRepository.save(newTranslate);
            }
        } else {
            throw new GlobalSystemError('Language already exist!');
            }
        }

        async getAllSupportedLanguages(): Promise<Language[]> {
            const allLanguages: Language[] = await this.languageRepository.find();

            return allLanguages;
        }

        async translateAllComponents(language: string): Promise<object> {
            const languageResult = await this.commonService.getCurrentLanguage(language);
            const allComponents: TranslationUI[] = await this.translationUiRepository.find({where: {targetLanguage: languageResult[1]}});
            const arrayOfValues = [];
            allComponents.forEach(el => {
                arrayOfValues.push(el.translatedText);
            });
            const component = {};

            // tslint:disable-next-line: forin
            for (const key in components) {
                const currentComponent = await this.translationUiRepository.findOne(
                    {where: {targetLanguage: languageResult[1], text: components[key]}});
                if (currentComponent) {
                    component[key] = currentComponent.translatedText;
                }
            }
            return component;
            }
}
