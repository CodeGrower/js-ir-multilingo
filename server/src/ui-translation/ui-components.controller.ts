import { Controller, Body, Post, Get, Param } from '@nestjs/common';
import { TranslationUiComponentsService } from './ui-components.service';
import { Language } from '../data/enitites/languages.entity';

@Controller('ui-components')
// @UseGuards(AuthGuardWithBlacklisting)
export class TranslationUiComponentsController {
    constructor(private readonly translationUiComponentService: TranslationUiComponentsService) {}

    @Post()
    async getComponentByLang(@Body() body: any): Promise<any> {
        const text = await this.translationUiComponentService.getComponentsByLang(body.text, body.langCode);
        return {text};
    }

    @Get()
    async getAllSupportedLanguages(): Promise<Language[]> {
        return this.translationUiComponentService.getAllSupportedLanguages();
    }

    @Get('/:language')
    async translateAllComponents(@Param('language') language: string): Promise<object> {
        return await this.translationUiComponentService.translateAllComponents(language);
    }
}
