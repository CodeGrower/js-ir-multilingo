import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../data/enitites/user.entity';
import { Article } from '../data/enitites/article.entity';
import { CreateArticleDTO } from './models/createArticleDTO';
import { ArticleVersionService } from '../article-version/article-version.service';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { VersionArticle } from '../data/enitites/article-version.entity';
import { plainToClass } from 'class-transformer';
import { ShowArticleDTO } from './models/showArticleDTO';
import { ShowArticleByIdDTO } from './models/ShowArticleByIdDTO';
import { Language } from '../data/enitites/languages.entity';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Injectable()
export class ArticlesService {
    constructor(
        @InjectRepository(Article) private readonly articleRepository: Repository<Article>,
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        private readonly articleVersionService: ArticleVersionService,
        private readonly commonService: CommonServiceService,
    ) {}

    async getAllArticles(langCode: string): Promise<ShowArticleDTO[]> {
        const articles: VersionArticle[] = await this.articleVersionService.getAllArticles(langCode);
        return plainToClass(ShowArticleDTO, articles, {
            excludeExtraneousValues: true,
        });
    }

    async getLastThreeArticles(langCode: string): Promise<ShowArticleDTO[]> {
        const articles: VersionArticle[] = await this.articleVersionService.getLastThreeArticles(langCode);
        return plainToClass(ShowArticleDTO, articles, {
            excludeExtraneousValues: true,
        });
    }

    async detectLanguage(body: CreateArticleDTO): Promise<any> {
        let obj = '';
        obj += body.title + '. ';
        obj += body.content;
        return this.articleVersionService.detectLanguage(obj);
    }

    async addNewArticle(body: CreateArticleDTO, user: User, origLang: string): Promise<any> {
        const newArticle: Article = this.articleRepository.create();
        newArticle.user = Promise.resolve(user);
        await this.articleRepository.save(newArticle);
        this.articleVersionService.createArticleOriginalVersion(body, newArticle, user, origLang);
    }

    async deleteArticle(artId: string = 'null', userId: string = 'null'): Promise<any> {
        if (userId !== 'null') {
            const articlesToDelete: Article[] = await this.articleRepository.find({where: {user: userId}});
            if (articlesToDelete === undefined) {
                throw new GlobalSystemError('No articles found!', 404);
              }
            Promise.all(articlesToDelete.map(async (article) => {
                article.isDeleted = true;
                this.articleVersionService.deleteArticleVersion(null, article.id);
                await this.articleRepository.save(article);
            }));
        } else if (artId !== 'null') {
            const articleToDelete: Article = await this.articleRepository.findOne({where: {id: artId}});
            if (articleToDelete === undefined) {
                throw new GlobalSystemError('No article found!', 404);
              }
            articleToDelete.isDeleted = true;
            await this.articleRepository.save(articleToDelete);
            this.articleVersionService.deleteArticleVersion(null, artId);
            }
        }

        async getArticleById(articleId: string, language: string): Promise<ShowArticleByIdDTO> {
            const languageResult = await this.commonService.getCurrentLanguage(language);
            return await this.articleVersionService.getArticleById(articleId, languageResult[1]);
        }
    }
