import { Controller, Get, Body, Post, Put, Param, UseGuards, ValidationPipe, createParamDecorator, Query } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { User } from '../data/enitites/user.entity';
import { ShowArticleDTO } from './models/showArticleDTO';
import { CreateArticleDTO } from './models/createArticleDTO';
export const InjectUser = createParamDecorator((_, req): User => req.user);

@Controller('articles')
export class ArticlesController {
    constructor(private readonly service: ArticlesService) {}

    @Get()
    async getAllArticles(@Query('language') language: string): Promise<ShowArticleDTO[]> {
        return await this.service.getAllArticles(language);
    }

    @Get('/:lastThree')
    // @ApiBearerAuth()
    async getLastThreeArticles(@Query('language') language: string): Promise<ShowArticleDTO[]> {
        const test = await this.service.getLastThreeArticles(language);
        return test;
    }

    @Post()
    @UseGuards(AuthGuardWithBlacklisting)
    async addNewArticle(
        @Body(new ValidationPipe({ whitelist: true, transform: true })) body: any, @InjectUser() user: User): Promise<ShowArticleDTO> {
        return await this.service.addNewArticle(body, user, body.origLang);
    }

    @Get('/:articleId/:language')
    async getArticleById(@Param('articleId') articleId: string, @Param('language') language: string) {
       return await this.service.getArticleById(articleId, language);
    }
    @Put('/:articleId/deleted')
    // @UseGuards(AdminGuard)
    async deleteArticle(@Param('articleId') articleId: string) {
        return await this.service.deleteArticle(articleId);
    }

    @Post('/detect')
    async detectLanguage(@Body() article: CreateArticleDTO): Promise<string> {
        return this.service.detectLanguage(article);
    }
}
