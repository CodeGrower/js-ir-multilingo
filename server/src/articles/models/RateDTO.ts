import { Expose } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';
export class RateDTO {

    @Expose()
    @IsNumber()
    public rating: number;

    @Expose()
    @IsString()
    public cookie: string;

    @Expose()
    @IsString()
    public content: string;
}
