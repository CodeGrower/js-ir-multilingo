import { IsEnum, IsString, IsBoolean, IsNumberString, MinLength, MaxLength } from 'class-validator';

export class ArticleDTO {

    @IsNumberString()
    public id: string;

    @MinLength(5, {
        message: 'Content is too short.',
    })
    @MaxLength(500, {
        message: 'The Content is too logn.',
    })
    public content: string;

    public title: string;

    @IsBoolean()
    public isDeleted: boolean;

}
