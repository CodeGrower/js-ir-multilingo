import { Expose, Transform } from "class-transformer";

export class ArticleUpdateDTO {
    @Expose()
   public id: string;

    @Expose()
   public content: string;
    @Expose()
   public title: string;

    @Expose()
    @Transform((_, obj) => obj.__article__.id)
   public articleId: string;
}
