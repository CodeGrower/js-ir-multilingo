import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

export class CreateArticleDTO {
    @Expose()
    @IsString()
    public title: string;

    @Expose()
    @IsString()
    public content: string;

    @IsString()
    public imgUrl: string;

    @IsString()
    public origLang: string;
}
