import { IsOptional, IsString } from 'class-validator';

export class UpdateTranslationDTO {
    @IsOptional()
    @IsString()
    public translatedText: string;
}
