import { Expose, Transform } from 'class-transformer';

import { MinLength, MaxLength, IsString } from 'class-validator';

export class ArticleVersionDTO {
    @Expose()
   public id: string;

    @Expose()
    public title: string;

    @Expose()
    @MinLength(5, {
        message: 'Title is too short.',
    })
    @MaxLength(50, {
        message: 'The title is too logn.',
    })
    public content: string;

    @Expose()
    @IsString()
    @Transform((_, obj) => obj.__article__.id)
    public originalArticleId: string;
}
