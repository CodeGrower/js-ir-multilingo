import { IsString, MinLength, MaxLength, IsNumber } from 'class-validator';
import { Expose, Transform } from 'class-transformer';

export class ShowArticleDTO {
    @Expose()
    public id: string;

    @Expose()
    public title: string;

    @Expose()
    @MinLength(5, {
        message: 'Title is too short.',
    })
    @MaxLength(50, {
        message: 'The title is too logn.',
    })
    public content: string;

    @Expose()
    @IsString()
    @Transform((_, obj) => obj.__user__.username)
    public author: string;

    @Expose()
    @IsString()
    @Transform((_, obj) => obj.__user__.avatarUrl)
    public authorImgUrl: string;

    @Expose()
    @IsString()
    public createdOn: string;

    @Expose()
    @IsString()
    public updatedOn: string;

    @Expose()
    @IsString()
    public imgUrl: string;

}
