import { Expose, Transform } from 'class-transformer';
import { IsString, IsNumber, IsArray } from 'class-validator';

export class ShowArticleByIdDTO {
    @Expose()
    public title: string;

    @Expose()
    public content: string;

    @Expose()
    @IsString()
    @Transform((_, obj) => obj.__user__.username)
    public author: string;

    @Expose()
    @IsString()
    @Transform((_, obj) => obj.__user__.avatarUrl)
    public authorImgUrl: string;

    @Expose()
    @IsString()
    public imgUrl: string;

    @Expose()
    @IsString()
    public createdOn: string;

    @Expose()
    @IsString()
    public updatedOn: string;

    @Expose()
    @IsNumber()
    public rating: number;

    @Expose()
    @IsArray()
    public rateCookies: any;
}
