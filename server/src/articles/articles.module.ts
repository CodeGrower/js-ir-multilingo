import { diskStorage } from 'multer';
import { Module } from '@nestjs/common/decorators/modules';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';
import { User } from '../data/enitites/user.entity';
import { extname } from 'path';
import { GlobalSystemError } from '../common/exceptions/global-system.error';
import { Article } from '../data/enitites/article.entity';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { VersionArticle } from '../data/enitites/article-version.entity';
import { TranslationRating } from '../data/enitites/rate.entity';
import { TranslationArticle } from '../data/enitites/translation-article.entity';
import { Language } from '../data/enitites/languages.entity';
import { ArticleVersionModule } from '../article-version/article-version.module';
import { CommonServiceService } from '../common/common-service/common-service.service';

@Module({
  imports: [TypeOrmModule.forFeature([Article, Language, TranslationRating, TranslationArticle, User, VersionArticle]),
  MulterModule.register({
    fileFilter(_, file, cb) {
      const ext = extname(file.originalname);
      const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];
      if (!allowedExtensions.includes(ext)) {
        return cb(
          new GlobalSystemError('Only images are allowed', 400),
          false,
        );
      }
      cb(null, true);
    },
    storage: diskStorage({
      destination: './booksImgs',
      filename: (_, file, cb) => {
        const randomName = Array.from({ length: 32 })
          .map(() => Math.round(Math.random() * 10))
          .join('');
        return cb(null, `${randomName}${extname(file.originalname)}`);
      },
    }),
  }), ArticleVersionModule],
  controllers: [ArticlesController],
  exports: [ArticlesService],
  providers: [ArticlesService, CommonServiceService],
})
export class ArticlesModule {}
