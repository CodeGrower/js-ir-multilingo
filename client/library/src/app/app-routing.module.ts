import { NgModule } from '@angular/core';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';

const appRoutes: Routes = [
  { path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
   {
    path: 'users',
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'translations',
    loadChildren: () => import('./translations/translations.module').then(m => m.TranslationsModule)
  }
];
const config: ExtraOptions = {
  onSameUrlNavigation: 'reload'
};
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes, config)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
