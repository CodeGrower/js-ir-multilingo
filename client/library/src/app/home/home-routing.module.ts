import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { RegisterComponent } from '../auth/register/register.component';
import { AnonymousGuard } from '../core/guards/anonymous.guard';
import { LoginComponent } from '../auth/login/login.component';
import { HomeComponent } from './home-component/home.component';
import { LastThreeArticlesResolver } from '../core/resolvers/getLastThreeArticles.resolver';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, pathMatch: 'full', resolve: {articles: LastThreeArticlesResolver} },
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class HomeRoutingModule {}
