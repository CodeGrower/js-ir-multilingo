import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../core/shared-module/shared.module';
import { HomeComponent } from './home-component/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent],
  imports: [SharedModule, CommonModule, FormsModule, HomeRoutingModule],
})
export class HomeModule {}
