import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ArticlesService } from '../../articles/articles.service';
import { ArticleDTO } from '../../articles/models/article.dto';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  public lastThreeArticles: any[] = [];
  public articlesImgUrls: string[] = [];
  public languageSubscription: Subscription;
  public ipAddress: any;
  public uiComponents = {
    MostRecentArticles: '',
    ReadMore: '',
  };
  constructor(
    private readonly router: Router,
    private readonly internationalizationService: InternationalizationService,
    private readonly route: ActivatedRoute
    ) {
      this.route.data.subscribe(({articles}) => {
        this.lastThreeArticles = articles;
        this.lastThreeArticles.forEach(el => {
          this.articlesImgUrls.push(el.imgUrl);
        });
      });
    }
  ngOnInit() {
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
        this.internationalizationService.getLastThreeArticles(lang).subscribe(data => {
          this.lastThreeArticles = data;
          this.lastThreeArticles.forEach(el => {
            this.articlesImgUrls.push(el.imgUrl);
          });
        });
      });
    });
  }
  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

  navigate(articleId: string): void {
    this.router.navigate([`/articles/${articleId}`]);
  }
}
