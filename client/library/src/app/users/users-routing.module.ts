import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from '../auth/register/register.component';
import { LoginComponent } from '../auth/login/login.component';
import { AnonymousGuard } from '../core/guards/anonymous.guard';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { AdminComponent } from './admin/admin.component';
import { LanguageResolver } from '../core/resolvers/language.resolver';
import { AllUsersResolver } from '../core/resolvers/getAllUsers.resolver';

const routes: Routes = [
  { path: 'register', component: RegisterComponent, pathMatch: 'full', resolve: {
    components: LanguageResolver
  }, canActivate: [AnonymousGuard] },
  { path: 'login', component: LoginComponent, pathMatch: 'full', resolve: {
    components: LanguageResolver
  }, canActivate: [AnonymousGuard] },
  { path: 'profile', component: ProfileComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'adminPanel', component: AdminComponent, pathMatch: 'full', resolve: {
    allUsers: AllUsersResolver
  }, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
