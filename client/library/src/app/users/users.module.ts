import { NgModule } from '@angular/core';

import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from '../core/shared-module/shared.module';

import { ProfileComponent } from './profile/profile.component';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from '../auth/register/register.component';
import { LoginComponent } from '../auth/login/login.component';

@NgModule({
  declarations: [ProfileComponent, AdminComponent, RegisterComponent, LoginComponent],
  imports: [
    SharedModule,
    UsersRoutingModule,
  ]
})
export class UsersModule { }
