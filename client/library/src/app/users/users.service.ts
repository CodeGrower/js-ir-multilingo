import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../config/config';
import { UserDTO } from './models/user.dto';
import { LanguageDTO } from './models/language-dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private readonly http: HttpClient) { }

  public uploadUserAvatar(userId: number, file: File): Observable<UserDTO> {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post<UserDTO>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/avatar`,
      formData
    );
  }
  public updateUser(userId: number, body: any) {
    return this.http.post<UserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/update`, body);
  }

  public deleteUser(userId: string) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/users/${userId}`, {});
  }

  public banUser(userId: string) {
      return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/users/${userId}/banned`, {});
  }

  public unBanUser(userId: string) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/users/${userId}/unbanned`, {});
}

  public getUserById(userId: number): Observable<UserDTO> {
    return this.http.get<UserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`);
  }

  public createEditor(userId: number): Observable<UserDTO> {
    return this.http.post<UserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/editor`, {});
  }

  public addNewLanguage(language: LanguageDTO): Observable<LanguageDTO[]> {
    return this.http.post<LanguageDTO[]>(`${CONFIG.DOMAIN_NAME}/users/language`, language);
  }
}
