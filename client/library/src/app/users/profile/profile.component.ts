import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { UsersService } from '../users.service';
import { CONFIG } from '../../config/config';
import { UserDTO } from '../models/user.dto';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  public closeResult: string;
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
    CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;
  private loggedUserSubscription: Subscription;
  public languageSubscription: Subscription;
  public uiComponents = {
    Name: '',
    Username: '',
    Surname: '',
    Email: '',
    ActiveRoles: '',
    UploadPicture: '',
    EditProfile: '',
    ProfileUpdate: '',
    Save: '',
  };
  private loggedUserData: UserDTO = {id: 0, username: '', avatarUrl: '', roles: [''], name: '', surname: '', email: ''};
  private username: string;
  private name: string;
  private surname: string;
  private email: string;
  private myModal: any;
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly internationalizationService: InternationalizationService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => {
        this.usersService.getUserById(data.id).subscribe((user: UserDTO) => this.loggedUserData = user);
      }
    );

    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
    });
  }

  ngOnDestroy() {
    if (this.loggedUserSubscription) {
      this.loggedUserSubscription.unsubscribe();
    }
    this.languageSubscription.unsubscribe();
  }
  public close() {
    this.myModal.close();
  }
  public onFileSelected(file: File): void {
    this.usersService
      .uploadUserAvatar(this.loggedUserData.id, file)
      .subscribe((updatedUser: UserDTO) => {
        this.authService.emitUserData(updatedUser);
      }
      );
  }

  public updateUser(): void {
    const updatedUser = {
      username: this.username,
      name: this.name,
      surname: this.surname,
      email: this.email
    };
    this.usersService.updateUser(this.loggedUserData.id, updatedUser).subscribe(data => {
      this.loggedUserData = data;
    });
  }
  public open(content): void {
    this.myModal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
    this.myModal.result.then();
  }
}
