export class CreateUserDTO {
    username: string;
    name: string;
    surname: string;
    email: string;
    password: string;
}
