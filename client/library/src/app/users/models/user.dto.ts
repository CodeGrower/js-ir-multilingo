export class UserDTO {
    id: number;
    name: string;
    surname: string;
    email: string;
    username: string;
    avatarUrl: string;
    roles: string[];
}
