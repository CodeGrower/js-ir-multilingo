export class LanguageDTO {
    language: string;
    code: string;
}
