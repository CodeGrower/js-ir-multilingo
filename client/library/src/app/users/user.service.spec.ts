import { UsersService } from "./users.service";
import { TestBed } from '@angular/core/testing';
import { ProfileComponent } from './profile/profile.component';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from '../auth/register/register.component';
import { LoginComponent } from '../auth/login/login.component';
import { SharedModule } from '../core/shared-module/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../config/config';
import { of } from 'rxjs';
import { UserDTO } from './models/user.dto';
import { LanguageDTO } from './models/language-dto';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UsersService', () => {
    const http = {
        post() {},
        delete() {},
        put() {},
        get() {}
      };
    let userService: () => UsersService;

    beforeEach(async () => {
        // clear all spies and mocks
        jest.clearAllMocks();

        TestBed.configureTestingModule({
            declarations: [ProfileComponent, AdminComponent, RegisterComponent, LoginComponent],
            imports: [
              SharedModule,
              UsersRoutingModule,
              HttpClientTestingModule
            ]

        })
        .overrideProvider(HttpClient, { useValue: http});

        userService = () => TestBed.get(UsersService);

});

    it(`uploadUserAvatar should call ${CONFIG.DOMAIN_NAME}/users/:userId/avatar with correct parameters`, () => {

        const formData = new FormData();
        const file = new File(['<html>…</html>'], 'fileName');
        const userId = 1;
        const spy = jest.spyOn(http, 'post');
        const service = userService();
        formData.append('file', file);

        service.uploadUserAvatar(userId, file);

        expect(http.post).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}/avatar`, formData);

});

    it(`uploadUserAvatar should return User`, (done) => {

        const user = new UserDTO();
        const formData = new FormData();
        const file = new File(['<html>…</html>'], 'fileName');
        const userId = 1;
        const spy = jest.spyOn(http, 'post').mockImplementation(() => of(user));
        const service = userService();
        formData.append('file', file);

        service.uploadUserAvatar(userId, file).subscribe(
            data => {
                expect(data).toBe(user);
                done();
            }
        );
});

    it(`updateUser should call ${CONFIG.DOMAIN_NAME}/users/:userId/update with correct parameters`, () => {

        const body = new UserDTO();
        const userId = 1;
        const spy = jest.spyOn(http, 'post');
        const service = userService();

        service.updateUser(userId, body);

        expect(http.post).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}/update`, body);

});

    it(`updateUser should return User`, (done) => {

        const user = new UserDTO();
        const body = new UserDTO();
        const userId = 1;
        const spy = jest.spyOn(http, 'post').mockImplementation(() => of(user));
        const service = userService();

        service.updateUser(userId, body).subscribe(
        data => {
            expect(data).toBe(user);
            done();
        }
    );
});

    it(`deleteUser should call ${CONFIG.DOMAIN_NAME}/users/:userId with correct parameters`, () => {

        const userId = '1';
        const spy = jest.spyOn(http, 'put');
        const service = userService();

        service.deleteUser(userId);

        expect(http.put).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}`, {});

});

    it(`deleteUser should return User`, (done) => {

        const user = new UserDTO();
        const userId = '1';
        const spy = jest.spyOn(http, 'put').mockImplementation(() => of(user));
        const service = userService();

        service.deleteUser(userId).subscribe(
        data => {
            expect(data).toBe(user);
            done();
        }
    );
});

    it(`banUser should call ${CONFIG.DOMAIN_NAME}/users/:userId/banned with correct parameters`, () => {

    const userId = '1';
    const spy = jest.spyOn(http, 'put');
    const service = userService();

    service.banUser(userId);

    expect(http.put).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}/banned`, {});

});

    it(`banUser should return User`, (done) => {

        const user = new UserDTO();
        const userId = '1';
        const spy = jest.spyOn(http, 'put').mockImplementation(() => of(user));
        const service = userService();

        service.banUser(userId).subscribe(
        data => {
            expect(data).toBe(user);
            done();
        }
    );
});

    it(`unBanUser should call ${CONFIG.DOMAIN_NAME}/users/:userId/unbanned with correct parameters`, () => {

        const userId = '1';
        const spy = jest.spyOn(http, 'put');
        const service = userService();

        service.unBanUser(userId);

        expect(http.put).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}/unbanned`, {});

});

    it(`unBanUser should return User`, (done) => {

        const user = new UserDTO();
        const userId = '1';
        const spy = jest.spyOn(http, 'put').mockImplementation(() => of(user));
        const service = userService();

        service.unBanUser(userId).subscribe(
        data => {
            expect(data).toBe(user);
            done();
        }
    );
});

    it(`getUserById should call ${CONFIG.DOMAIN_NAME}/users/:userId with correct parameters`, () => {

        const userId = 1;
        const spy = jest.spyOn(http, 'get');
        const service = userService();

        service.getUserById(userId);

        expect(http.get).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}`);

});

    it(`getUserById should return User`, (done) => {

        const user = new UserDTO();
        const userId = 1;
        const spy = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const service = userService();

        service.getUserById(userId).subscribe(
        data => {
            expect(data).toBe(user);
            done();
        }
    );
});

    it(`createEditor should call ${CONFIG.DOMAIN_NAME}/users/:userId/editor with correct parameters`, () => {

        const userId = 1;
        const spy = jest.spyOn(http, 'post');
        const service = userService();

        service.createEditor(userId);

        expect(http.post).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/${userId}/editor`, {});

});

    it(`createEditor should return User`, (done) => {

        const user = new UserDTO();
        const userId = 1;
        const spy = jest.spyOn(http, 'post').mockImplementation(() => of(user));
        const service = userService();

        service.createEditor(userId).subscribe(
        data => {
            expect(data).toBe(user);
            done();
        }
    );
});

    it(`addNewLanguage should call ${CONFIG.DOMAIN_NAME}/users/language with correct parameters`, () => {

        const language = new LanguageDTO();
        const spy = jest.spyOn(http, 'post');
        const service = userService();

        service.addNewLanguage(language);

        expect(http.post).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/users/language`, language);

});

    it(`addNewLanguage should return Language`, (done) => {

    const language = new LanguageDTO();
    const spy = jest.spyOn(http, 'post').mockImplementation(() => of(language));
    const service = userService();

    service.addNewLanguage(language).subscribe(
    data => {
        expect(data).toBe(language);
        done();
    }
);
});

});
