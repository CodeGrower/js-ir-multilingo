import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CONFIG } from '../../config/config';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';
import { LanguageDTO } from '../models/language-dto';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public users = [];
  public p = 1;
  public openedModal;
  public userAvatarSrcPrefix = CONFIG.USER_AVATAR_SRC_PREFIX;
  public languageSubscription: Subscription;
  public uiComponents = {
    RegisteredUsers: '',
    ViewDetails: '',
    Username: '',
    Name: '',
    Surname: '',
    Email: '',
    ActiveRoles: '',
    DeleteUser: '',
    BanUser: '',
    UnbanUser: '',
    UpdateUser: '',
    SetEditorRole: '',
    CreateNewLanguage: '',
    Language: '',
    LanguageCode: '',
    Save: '',
    BanQuestion: '',
    DeleteQuestion: '',
    UnbanQuestion: '',
  };
  public username: string;
  public name: string;
  public surname: string;
  public email: string;
  public language: string;
  public languageCode: string;
  constructor(
    private readonly usersService: UsersService,
    private readonly modalService: NgbModal,
    private readonly route: ActivatedRoute,
    private readonly internationalizationService: InternationalizationService,
    ) {

      this.route.data.subscribe(({allUsers}) => {
        this.users = allUsers;
      });
    }

    ngOnInit() {
this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
          this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
            this.internationalizationService.swap(this.uiComponents, translatedComponents);
        });
      });
    }

  close(): void {
    this.openedModal.close();
  }
  open(content): void {
      this.openedModal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
      this.openedModal.result.then();
  }
  onDeleteButton(userId: string, userName: string): void {
    if (confirm(`${this.uiComponents.DeleteQuestion} ${userName}`)) {
      this.usersService.deleteUser(userId).subscribe(() => {
        const index = this.users.findIndex(x => x.id === userId);
        this.users.splice(index, 1);
      });
    }
  }
  onBanButton(userId: string, userName: string): void {
    if (confirm(`${this.uiComponents.BanQuestion} ${userName}`)) {
      this.usersService.banUser(userId).subscribe(() => {
        const index = this.users.findIndex(x => x.id === userId);
        this.users[index].isBanned = true;
      });
    }
  }

  onUnBanButton(userId: string, userName: string): void {
    if (confirm(`${this.uiComponents.UnbanQuestion} ${userName}`)) {
      this.usersService.unBanUser(userId).subscribe(data => {
        const index = this.users.findIndex(x => x.id === userId);
        this.users[index].isBanned = false;
      });
  }
}
  createEditor(userId: number): void {
    this.usersService.createEditor(userId).subscribe(data => {
      const index = this.users.findIndex(x => x.id === userId);
      this.users[index] = data;
    });
  }
  updateUser(userId: number): void {
    const updatedUser = {
      username: this.username,
      name: this.name,
      surname: this.surname,
      email: this.email
    };
    this.usersService.updateUser(userId, updatedUser).subscribe(data => {
        const index = this.users.findIndex(x => x.id === userId);
        this.users[index] = data;
  });
  }

  addNewLanguage(): void {
    const newLanguage: LanguageDTO = {
      language: this.language,
      code: this.languageCode
    };
    this.usersService.addNewLanguage(newLanguage).subscribe(data => {
      this.internationalizationService.updateAllLanguages(data);
    });
  }
}
