import { AllArticlesComponent } from './components/all-articles/all-articles.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ArticlePreviewComponent } from './components/article-preview/article-preview.component';
import { CreateArticleComponent } from './components/create-article/create-article.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { MyArticlesComponent } from './components/my-articles/my-articles.component';
import { MyArticlesPreviewComponent } from './components/my-articles-preview/my-articles-preview.component';
import { MyArticlesEditComponent } from './components/my-articles-edit/my-articles-edit.component';
import { LanguageResolver } from '../core/resolvers/language.resolver';
import { ArticlesByLanguage } from '../core/resolvers/articlesByLanguage.resolver';
import { UsersArticleVersionsResolver } from '../core/resolvers/userArticleVersions.resolver';
import { ArticleByIdResolver } from '../core/resolvers/articleById.resolver';
import { ArticleResolver } from '../core/resolvers/getArticle.resolver';
import { AllArticleVersionsResolver } from '../core/resolvers/getAllArticleVersions.resolver';

const routes: Routes = [
    { path: 'articles', component: AllArticlesComponent, pathMatch: 'full', resolve: {
      articles: ArticlesByLanguage, components: LanguageResolver}},

    { path: 'newArticle', component: CreateArticleComponent, pathMatch: 'full', canActivate: [AuthGuard]},

    { path: 'myArticles', component: MyArticlesComponent, pathMatch: 'full', resolve: {
      versions: UsersArticleVersionsResolver
    }, canActivate: [AuthGuard]},

    { path: 'editArticle/:id', component: MyArticlesEditComponent, pathMatch: 'full',
    resolve: { article: ArticleResolver }, canActivate: [AuthGuard]},

    { path: 'myArticlesPreview/:id', component: MyArticlesPreviewComponent, pathMatch: 'full',
    resolve: { versions: AllArticleVersionsResolver }, canActivate: [AuthGuard]},

    { path: 'articles/:id', component: ArticlePreviewComponent, pathMatch: 'full', resolve: {
     components: LanguageResolver, article: ArticleByIdResolver }},

    { path: 'search/:title', component: AllArticlesComponent, pathMatch: 'full', resolve: {
      articles: ArticlesByLanguage, components: LanguageResolver}},
  ];

@NgModule({
    declarations: [],
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
  })
export class ArticlesRoutingModule { }

