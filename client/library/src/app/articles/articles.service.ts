import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../config/config';
import { ArticleDTO } from './models/article.dto';
import { Observable } from 'rxjs';
import { CreateArticleDTO } from './models/create-article.dto';
import { RateDTO } from './models/rate.dto';
import { ArticleUpdateDTO } from './models/ArticleUpdateDTO';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private readonly http: HttpClient) { }

  createArticle(body: any): Observable<ArticleDTO> {
    const language = navigator.language;
    body.language = language;
    return this.http.post<ArticleDTO>(`${CONFIG.DOMAIN_NAME}/articles`, body);
  }

  getIpAddress(): Observable<any> {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/session/ipAddress`);
  }

  rateTranslation(body: RateDTO, language: string) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/rate/${language}`, body);
  }

  deleteArticleVersion(artId: string) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/article-version/${artId}/delete`, artId);
  }

  deleteAllArticleVersions(artId: string) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/articles/${artId}/deleted`, artId);
  }

  setCurrentArticleVersion(artId: string) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/article-version/${artId}/current`, artId);
  }

  detectLanguage(body: CreateArticleDTO): Observable<any> {
    return this.http.post<any>(`${CONFIG.DOMAIN_NAME}/articles/detect`, body);
  }

  updateArticleVersion(artId: string, body: ArticleUpdateDTO) {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/article-version/${artId}`, body);
  }
}
