export class ArticleUpdateDTO {
    id: string;
    content: string;
    title: string;
    articleId: string;
}
