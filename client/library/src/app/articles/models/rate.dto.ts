export class RateDTO {
    rating: number;
    cookie: string;
    content: string;
}
