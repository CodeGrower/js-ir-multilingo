export class UserDetailsDTO {

    id: string;

    username: string;

    password: string;

    isBanned: boolean;

    isDeleted: boolean;
}
