export class ArticleDTO {
    title: string;
    content: string;
    author: string;
    authorImgUrl: string;
    imgUrl: string;
    updatedOn: string;
    rating: number;
    rateCookies: object;
}
