export class CreateArticleDTO {
    public title: string;
    public content: string;
    public imgUrl: string;
    public origLang: string;
}
