import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { CommonModule } from '@angular/common';
import { ArticlesService } from './articles.service';
import { CONFIG } from '../config/config';
import { CreateArticleDTO } from './models/create-article.dto';
import { SharedModule } from '../core/shared-module/shared.module';
import { ArticlesRoutingModule } from './articles-routing.module';
import { AllArticlesComponent } from './components/all-articles/all-articles.component';
import { ArticlePreviewComponent } from './components/article-preview/article-preview.component';
import { CreateArticleComponent } from './components/create-article/create-article.component';
import { MyArticlesComponent } from './components/my-articles/my-articles.component';
import { MyArticlesPreviewComponent } from './components/my-articles-preview/my-articles-preview.component';
import { MyArticlesEditComponent } from './components/my-articles-edit/my-articles-edit.component';
import { HttpClient } from '@angular/common/http';
import { ArticleDTO } from './models/article.dto';
import { of } from 'rxjs';
import { RateDTO } from './models/rate.dto';
import { ArticleUpdateDTO } from './models/ArticleUpdateDTO';

describe('ArticlesService', () => {
        const http = {
            post() {},
            delete() {},
            put() {},
            get() {}
          };
        let getService: () => ArticlesService;

        beforeEach(async () => {
            // clear all spies and mocks
            jest.clearAllMocks();

            TestBed.configureTestingModule({
              imports: [
                ImageCropperModule,
                NgxStarRatingModule,
                SharedModule,
                ArticlesRoutingModule,
                CommonModule,
                HttpClientTestingModule
            ],
              declarations: [
                AllArticlesComponent,
                ArticlePreviewComponent,
                CreateArticleComponent,
                MyArticlesComponent,
                MyArticlesPreviewComponent,
                MyArticlesEditComponent,
            ],
              providers: [],
            })
            .overrideProvider(HttpClient, { useValue: http });

            getService = () => TestBed.get(ArticlesService);

          });

        it(`createArticle should call ${CONFIG.DOMAIN_NAME}/articles with correct parameters`, () => {
            const article = new CreateArticleDTO();
            const spy = jest.spyOn(http, 'post');
            const service = getService();
            service.createArticle(article);
            expect(http.post).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/articles`, article);
        });

        it(`createArticle should return new Article`, (done) => {
            const article = new CreateArticleDTO();
            // tslint:disable-next-line:prefer-const
            let newArticle: ArticleDTO;
            const spy = jest.spyOn(http, 'post').mockImplementation(() => of(newArticle));
            const service = getService();
            service.createArticle(article).subscribe(
                data => {
                    expect(data).toBe(newArticle);
                    done();
                }
            );
        });

        it(`rateTranslation should call ${CONFIG.DOMAIN_NAME}/rate/:language with correct parameters and return correct value`, (done) => {
            const rate = new RateDTO();
            const language = 'bg';
            // tslint:disable-next-line:prefer-const
            const spy = jest.spyOn(http, 'put').mockImplementation(() => of(rate));
            const service = getService();
            service.rateTranslation(rate, language).subscribe(
                data => {
                    expect(data).toBe(rate);
                    done();
                }
            );
        });

        // it(`getLastThreeArticles should return last three articles`, (done) => {
        //     const langCode = 'bg';
        //     // tslint:disable-next-line:prefer-const
        //     let articles: ArticleDTO[];
        //     const spy = jest.spyOn(http, 'get').mockImplementation(() => of(articles));
        //     const service = getService();
        //     service.getLastThreeArticles(langCode).subscribe(
        //         data => {
        //             expect(data).toBe(articles);
        //             done();
        //         }
        //     );
        // });

        it(`deleteArticleVersion should call ${CONFIG.DOMAIN_NAME}/article-version/:artId/delete`, () => {
            const artId = '1';
            const spy = jest.spyOn(http, 'put');
            const service = getService();
            service.deleteArticleVersion(artId);
            expect(http.put).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/article-version/${artId}/delete`, artId);
        });

        it(`setCurrentArticleVersion should call ${CONFIG.DOMAIN_NAME}/article-version/:artId/current`, () => {
            const artId = '1';
            const spy = jest.spyOn(http, 'put');
            const service = getService();
            service.setCurrentArticleVersion(artId);
            expect(http.put).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/article-version/${artId}/current`, artId);
        });

        it(`detectLanguage should call ${CONFIG.DOMAIN_NAME}/articles/detect with correct params` , () => {
            const body = new CreateArticleDTO();
            const spy = jest.spyOn(http, 'post');
            const service = getService();
            service.detectLanguage(body);
            expect(http.post).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/articles/detect`, body);
        });

        it(`detectLanguage should return language code` , (done) => {
            const body = new CreateArticleDTO();
            const langCode = '1';
            const spy = jest.spyOn(http, 'post').mockImplementation(() => of({langCode}));
            const service = getService();
            service.detectLanguage(body).subscribe(data => {
                expect(data.langCode).toBe(langCode);
                done();
            });
        });

        it(`updateArticleVersion should call ${CONFIG.DOMAIN_NAME}/article-version/:artId with correct parameters`, () => {
            const article = new ArticleUpdateDTO();
            const articleId = '1';
            const spy = jest.spyOn(http, 'put');
            const service = getService();
            service.updateArticleVersion(articleId, article);
            expect(http.put).toHaveBeenCalledWith(`${CONFIG.DOMAIN_NAME}/article-version/${articleId}`, article);
        });

        it(`updateArticleVersion should return updatedArticle`, (done) => {
            const article = new ArticleUpdateDTO();
            const articleId = '1';
            const spy = jest.spyOn(http, 'put').mockImplementation(() => of({article}));
            const service = getService();
            service.updateArticleVersion(articleId, article).subscribe(data => {
                expect(data.article).toBe(article);
                done();
            });
        });

});
