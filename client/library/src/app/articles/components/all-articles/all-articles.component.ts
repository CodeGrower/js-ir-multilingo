import { OnInit, DoCheck, OnDestroy, Component } from '@angular/core';
import { ArticleDTO } from '../../models/article.dto';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { UserDTO } from '../../../users/models/user.dto';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../../core/services/internationalization.service';

@Component({
  selector: 'app-all-articles',
  templateUrl: './all-articles.component.html',
  styleUrls: ['./all-articles.component.css']
})
export class AllArticlesComponent implements OnInit, OnDestroy, DoCheck {
  private user: UserDTO;
  private languageSubscription: Subscription;
  private articleSubscription: Subscription;
  private componentsSubscription: Subscription;
  public articles: ArticleDTO[];
  public p = 1;
  public uiComponents = {
    CreateNewArticle: '',
    ReadMore: '',
  };
    newArr = [];

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly internationalizationService: InternationalizationService,
    ) {
      this.route.data.subscribe(({articles, components}) => {
    this.internationalizationService.swap(this.uiComponents, components);
    this.articles = [...articles];
    this.newArr = [...articles];
  });
    }

  ngOnInit() {
    this.user = this.authService.getUserDataIfAuthenticated();
    this.route.data.subscribe(({articles, components}) => {
      this.internationalizationService.swap(this.uiComponents, components);
      this.articles = [...articles];
      this.newArr = [...articles];
    });
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
          this.internationalizationService.translateAllComponents(lang).subscribe((components) => {
            this.internationalizationService.swap(this.uiComponents, components);
            this.internationalizationService.getAllArticlesByLangCode(lang).subscribe((translations) => {
              this.articles = translations;
              this.newArr = translations;
            });
          });
        });
  }

  ngDoCheck() {
    this.route.params.subscribe(params => {
      if (this.newArr.length !== 0) {
        this.articles = this.newArr;
        if (params.title) {
       this.articles = this.articles.filter(el => el.title.toLowerCase().includes(params.title.toLowerCase()));
      }
    }
    });
  }

  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

  onDetailsClick(articleId: string): void {
    this.router.navigate([`/articles/${articleId}`]);
  }

  createArticle(): void {
    this.router.navigate(['/newArticle']);
  }
}

