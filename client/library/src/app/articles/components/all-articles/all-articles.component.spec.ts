import { CreateArticleComponent } from '../create-article/create-article.component';
import { AllArticlesComponent } from './all-articles.component';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { TestBed, ComponentFixture, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../../auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { SharedModule } from '../../../core/shared-module/shared.module';
import { ArticlesRoutingModule } from '../../articles-routing.module';
import { CommonModule } from '@angular/common';
import { ArticlePreviewComponent } from '../article-preview/article-preview.component';
import { MyArticlesComponent } from '../my-articles/my-articles.component';
import { MyArticlesPreviewComponent } from '../my-articles-preview/my-articles-preview.component';
import { MyArticlesEditComponent } from '../my-articles-edit/my-articles-edit.component';
import { ArticlesService } from '../../articles.service';
import { InternationalizationService } from '../../../core/services/internationalization.service';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { ArticleDTO } from '../../models/article.dto';
import { LanguageResolver } from '../../../core/resolvers/language.resolver';
import { ArticleByIdResolver } from '../../../core/resolvers/articleById.resolver';

describe('AllArticlesComponent', () => {
        const routes: Routes = [
            { path: 'articles', component: AllArticlesComponent , pathMatch: 'full' },
            { path: 'newArticle', component: CreateArticleComponent, pathMatch: 'full' },
          ];
        const authService = {
            getUserDataIfAuthenticated() {}
        };
        const http = {
            post() {},
            delete() {},
            put() {},
            get() {}
          };
        const articleService = {
            getAllArticlesByLangCode: (articles) => of(
                articles,
              )
        };

        const languageResolver = {
          resolve() {}
        };

        const articleByIdResolver = {
          resolve() {}
        };


        const activatedRoute = {
          // default initialize with empty array of heroes
          data: of({
            articles: {}
          }),

          params: of({})
        };
        const internationalizationService = {
            getLanguage: () => of({
                text: '',
              }),
            translateAllComponents() {},
            swap() {}
        };
        let router: Router;
        let fixture: ComponentFixture<AllArticlesComponent>;
        let component: AllArticlesComponent;

        beforeEach(async () => {
              // clear all spies and mocks
          jest.clearAllMocks();

          TestBed.configureTestingModule({
              imports: [
                RouterTestingModule.withRoutes(routes),
                ImageCropperModule,
                NgxStarRatingModule,
                SharedModule,
                ArticlesRoutingModule,
                CommonModule,
                HttpClientTestingModule
            ],
              declarations: [
                AllArticlesComponent,
                ArticlePreviewComponent,
                CreateArticleComponent,
                MyArticlesComponent,
                MyArticlesPreviewComponent,
                MyArticlesEditComponent,
            ],
              providers: [
                LanguageResolver,
                ArticleByIdResolver
              ],
            })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(InternationalizationService, { useValue: internationalizationService })
            .overrideProvider(articleService, { useValue: articleService })
            .overrideProvider(HttpClient, { useValue: http })
            .overrideProvider(ActivatedRoute, { useValue: activatedRoute})
            .overrideProvider(LanguageResolver, { useValue: languageResolver})
            .overrideProvider(ArticleByIdResolver, { useValue: articleByIdResolver});

          router = TestBed.get(Router);
          fixture = TestBed.createComponent(AllArticlesComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
          });

        it('should component be defined', () => {
            expect(AllArticlesComponent).toBeDefined();
        });

        it('should return the article data if it exists', (done) => {
            const articles = [];
            const spy = jest.spyOn(articleService, 'getAllArticlesByLangCode').mockImplementation(() => of(articles));
            articleService.getAllArticlesByLangCode(articles)
              .subscribe(
                data => {
                  expect(data).toBe(articles);
                  done();
                }
              );
          });

        it('should initialize correctly with the data passed from the resolver', () => {
          activatedRoute.data.subscribe(data => {
            expect(component.articles).toBe(data);
          });
        });

        it('onDetailsClick should navigate to /articles route with correct params', () => {
        const spy = jest.spyOn(router, 'navigate');
        const articleId = '1';
        fixture.ngZone.run(() => {
          component.onDetailsClick(articleId);
          fixture.detectChanges();
          fixture.whenStable().then(() => {
              expect(router.navigate).toHaveBeenCalledWith([`/articles/${articleId}`]);
          });
      });
      });

        it('createArticle should navigate to /newArticle route', () => {
          const spy = jest.spyOn(router, 'navigate');
          const articleId = '1';
          fixture.ngZone.run(() => {
            component.createArticle();
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                expect(router.navigate).toHaveBeenCalled();
            });
        });
        });

});
