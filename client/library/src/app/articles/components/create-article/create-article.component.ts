import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import { ArticlesService } from '../../articles.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../core/services/notification.service';
import { Router } from '@angular/router';
import { CreateArticleDTO } from '../../models/create-article.dto';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../../core/services/internationalization.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})

export class CreateArticleComponent implements OnInit, OnDestroy {
  @Input() public articleTitle: string;
  @Input() public articleContent: string;
  public imageChangedEvent: any = false;
  public imgUpdatedUrl: any = '';
  public articleForm: FormGroup;
  public languageCode: string;
  public uiComponents = {
    Title: '',
    InputInfo: '',
    EnterTitle: '',
    EnterContent: '',
    Content: '',
    ContentInfo: '',
    Submit: '',
    LanguageConfirm: '',
    LanguageConfirmQuestion: '',
    Save: '',
    CreatedArticle: '',
  };
  private languageSubscription: Subscription;
  @ViewChild('content', {static: false}) myModal: any;
  constructor(
    private readonly articlesService: ArticlesService,
    private readonly http: HttpClient,
    private readonly notificator: NotificationService,
    private readonly internationalizationService: InternationalizationService,
    private readonly route: Router,
    private readonly fb: FormBuilder,
    public modalService: NgbModal
  ) {
    this.articleForm = this.fb.group({
    url: ['', [Validators.required]],
    title: ['', [Validators.required, Validators.minLength(10)]],
    content: ['', [Validators.required, Validators.minLength(50)]],
  });
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then();
  }
  imageCropped(event: ImageCroppedEvent) {
    this.imgUpdatedUrl = event.base64;
  }
  ngOnInit() {
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
    });
  }
  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }
  onFileUpdate(event: any) {
    this.imageChangedEvent = event;
  }
  save() {
    const createdArticle: CreateArticleDTO = {
      title: this.articleTitle,
      content: this.articleContent,
      imgUrl: this.imgUpdatedUrl,
      origLang: this.languageCode,
    };
    this.articlesService.createArticle(createdArticle).subscribe(() => {
        this.notificator.success(`${this.uiComponents.CreatedArticle}`);
        this.route.navigate(['/home']);
      });
  }
  onCreateArticleButton(): void {
      this.imgUpdatedUrl = this.imgUpdatedUrl.replace('data:image/png;base64,', '');
      this.http.post<any>('https://api.imgur.com/3/image', this.imgUpdatedUrl).subscribe((data) => {
        this.imgUpdatedUrl = data.data.link;
        const createdArticle: CreateArticleDTO = {
          title: this.articleTitle,
          content: this.articleContent,
          imgUrl: this.imgUpdatedUrl,
          origLang: navigator.language,
        };
        this.articlesService.detectLanguage(createdArticle).subscribe(realLang => {
          this.languageCode = realLang[0].language;
          this.open(this.myModal);
        });
      });
    }
}
