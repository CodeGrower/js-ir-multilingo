import { Routes, Router, ActivatedRoute } from '@angular/router';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MyArticlesPreviewComponent } from './my-articles-preview.component';
import { of, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { SharedModule } from '../../../core/shared-module/shared.module';
import { ArticlesRoutingModule } from '../../articles-routing.module';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ArticlePreviewComponent } from '../article-preview/article-preview.component';
import { CreateArticleComponent } from '../create-article/create-article.component';
import { MyArticlesComponent } from '../my-articles/my-articles.component';
import { MyArticlesEditComponent } from '../my-articles-edit/my-articles-edit.component';
import { ArticlesService } from '../../articles.service';
import { InternationalizationService } from '../../../core/services/internationalization.service';
import { NotificationService } from '../../../core/services/notification.service';
import { AllArticlesComponent } from '../all-articles/all-articles.component';
import { ArticlesByLanguage } from '../../../core/resolvers/articlesByLanguage.resolver';
import { LanguageResolver } from '../../../core/resolvers/language.resolver';

describe('MyArticlesPreviewComponent', () => {
    const routes: Routes = [
        { path: 'articles', component: AllArticlesComponent, pathMatch: 'full' },
    ];
    const articleService = {
        deleteArticleVersion: (articleId) => of(
            articleId
        ),
        setCurrentArticleVersion: (articleId) => of(
            articleId,
        )
    };
    const articlesByLanguage = {
        getAllArticlesByLangCode() { },
        resolve() { }
    };
    const languageResolver = {
        getAllArticlesByLangCode() { },
        resolve() { }
    };
    const notificationService = {
        success() { },
        error() { },
    };
    const internationalizationService = {
        getAllArticleVersions: (articleId) => of(
            articleId,
        ),
        translateAllComponents: (components) => of(
            components
        ),
        swap: (object, components) => of(
            object, components,
        ),
        getLanguage: (language) => of(
            language
        ),
    };
    const activatedRoute = {
        data: of({
            versions: [],
        }),

        snapshot: {
            params: [{ id: 'id' }],
        }
    };

    let router: Router;
    let fixture: ComponentFixture<MyArticlesPreviewComponent>;
    let component: MyArticlesPreviewComponent;
    beforeEach(async () => {
        jest.clearAllMocks();
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                ImageCropperModule,
                NgxStarRatingModule,
                SharedModule,
                ArticlesRoutingModule,
                CommonModule,
                HttpClientTestingModule
            ],
            declarations: [
                AllArticlesComponent,
                ArticlePreviewComponent,
                CreateArticleComponent,
                MyArticlesComponent,
                MyArticlesPreviewComponent,
                MyArticlesEditComponent,
            ],
            providers: [ArticlesByLanguage, LanguageResolver],
        })
            .overrideProvider(ArticlesService, { useValue: articleService })
            .overrideProvider(InternationalizationService, { useValue: internationalizationService })
            .overrideProvider(NotificationService, { useValue: notificationService })
            .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
            .overrideProvider(ArticlesByLanguage, { useValue: articlesByLanguage })
            .overrideProvider(LanguageResolver, { useValue: languageResolver });


        router = TestBed.get(Router);
        fixture = TestBed.createComponent(MyArticlesPreviewComponent);
        component = fixture.componentInstance;
    });

    it('should component to be defined', () => {
        expect(MyArticlesPreviewComponent).toBeDefined();
    });

    it('should component to be truthy', () => {
        expect(MyArticlesPreviewComponent).toBeTruthy();
    });

    it('shoul set current article if article exist', () => {
        const articleId = '1';
        const article = {};
        component.warningFlag = true;
        const spy = jest.spyOn(articleService, 'setCurrentArticleVersion').mockImplementation(() => of(article));
        articleService.setCurrentArticleVersion(articleId)
            .subscribe(data => data.subscribe(data1 =>
                expect(data1).toEqual(article)
            ));
    });

    it('should call the nottificator service on success', () => {
        const articleId = '1';
        const article = {};
        component.warningFlag = true;
        const spy = jest.spyOn(notificationService, 'success');
        jest.spyOn(articleService, 'setCurrentArticleVersion').mockImplementation(() => of(article));
        articleService.setCurrentArticleVersion(articleId);
        component.deleteArticle(articleId);
        expect(spy).toHaveBeenCalled();
    });

    it('should call the nottificator service on error', () => {
        const articleId = '1';
        const article = {};
        component.warningFlag = true;
        const spy = jest.spyOn(notificationService, 'error');
        jest.spyOn(articleService, 'setCurrentArticleVersion').mockImplementation(() => throwError('error'));
        component.setCurrentVersion('1');
        expect(spy).toHaveBeenCalled();
    });
    describe('deleteArticle()', () => {

        it('should component to be defined', () => {
            expect(MyArticlesPreviewComponent).toBeDefined();
        });

        it('should component to be truthy', () => {
            expect(MyArticlesPreviewComponent).toBeTruthy();
        });

        it('shoul deleteArticle() if article exist', () => {
            const articleId = '1';
            const article = {};
            const spy = jest.spyOn(articleService, 'deleteArticleVersion').mockImplementation(() => of(articleId));
            articleService.deleteArticleVersion(articleId)
                .subscribe(data => {
                    expect(data).toBe(article);
                });
        });

        it('should call the nottificator service on success', () => {
            const articleId = '1';
            const article = {};
            component.warningFlag = true;
            const spy = jest.spyOn(notificationService, 'success');
            jest.spyOn(articleService, 'setCurrentArticleVersion').mockImplementation(() => of(article));
            articleService.setCurrentArticleVersion(articleId);
            component.deleteArticle(articleId);
            expect(spy).toHaveBeenCalled();
        });
    });

    it('should call the nottificator service on error', () => {
        const articleId = '1';
        component.warningFlag = true;
        const spy = jest.spyOn(notificationService, 'error');
        jest.spyOn(articleService, 'setCurrentArticleVersion').mockImplementation(() => throwError('error'));
        component.setCurrentVersion(articleId);
        expect(spy).toHaveBeenCalled();
    });

});
