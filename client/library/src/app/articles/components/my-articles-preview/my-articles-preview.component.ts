import { Component, OnInit } from '@angular/core';
import { ArticleDTO } from '../../models/article.dto';
import { ArticlesService } from '../../articles.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../core/services/notification.service';
import { InternationalizationService } from '../../../core/services/internationalization.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-my-articles-preview',
  templateUrl: './my-articles-preview.component.html',
  styleUrls: ['./my-articles-preview.component.css']
})
export class MyArticlesPreviewComponent implements OnInit {
public myArticleVersions: ArticleDTO[] = [];
public warningFlag = true;
public uiComponents = {
  DeleteArticle: '',
  Title: '',
  Content: '',
  Rating: '',
  MyArticles: '',
  ShowVersions: '',
  SetVersion: '',
  GoBack: '',
  ArticleSet: '',
  ArticleSetFail: '',
  ArticleDelete: '',
  ArticleDeleteFail: '',
  ArticleVersionInfo: '',
};
private languageSubscription: Subscription;
private articleId: string;
  constructor(
    private readonly articleService: ArticlesService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
    private readonly internationalizationService: InternationalizationService,
  ) {
    this.route.data.subscribe(({versions}) => {
      this.myArticleVersions = versions;
    });
  }
  ngOnInit() {
    // tslint:disable-next-line: no-string-literal
    this.articleId = this.route.snapshot.params['id'];
    this.internationalizationService.getAllArticleVersions(this.articleId).subscribe(data => {
      this.myArticleVersions = data;
    });
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((components) => {
        this.internationalizationService.swap(this.uiComponents, components);
      });
    });
  }

  setCurrentVersion(Id: string) {
    this.warningFlag = false;
    this.articleService.setCurrentArticleVersion(Id).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.ArticleSet}`);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.ArticleSetFail}`);
    });
  }

  deleteArticle(articleId: string): void {
    this.articleService.deleteArticleVersion(articleId).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.ArticleDelete}`);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.ArticleDeleteFail}`);
    });
  }

  goBack(): void {
    this.router.navigate(['/articles']);
  }

}
