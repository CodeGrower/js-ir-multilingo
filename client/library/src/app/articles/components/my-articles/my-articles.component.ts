import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../../auth/auth.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../users/models/user.dto';
import { UsersService } from '../../../users/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticlesService } from '../../articles.service';
import { ArticleDTO } from '../../models/article.dto';
import { NotificationService } from '../../../core/services/notification.service';
import { InternationalizationService } from '../../../core/services/internationalization.service';

@Component({
  selector: 'app-my-articles',
  templateUrl: './my-articles.component.html',
  styleUrls: ['./my-articles.component.css']
})
export class MyArticlesComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public allArticles: ArticleDTO[];
  public uiComponents = {
    DeleteArticle: '',
    Title: '',
    Content: '',
    Rating: '',
    MyArticles: '',
    ShowVersions: '',
    EditArticle: '',
    ArticleDelete: '',
    ArticleDeleteFail: '',
    GoBack: '',
  };
  private loggedUserData: UserDTO;
  private languageSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly articleService: ArticlesService,
    private readonly notificationService: NotificationService,
    private readonly internationalizationService: InternationalizationService,
    ) {
      this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
        (data: UserDTO) => {
          this.usersService.getUserById(data.id).subscribe((user: UserDTO) => this.loggedUserData = user);
        });
    }

  ngOnInit() {
    this.route.data.subscribe(({versions}) => {
      this.allArticles = versions;
    });
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((components) => {
        this.internationalizationService.swap(this.uiComponents, components);
      });
    });
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }

  deleteAllArticles(articleId: string): void {
    this.articleService.deleteAllArticleVersions(articleId).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.ArticleDelete}`);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.ArticleDeleteFail}`);
    });
  }

  viewArticleDetails(articleId: string): void {
    this.router.navigate([`/myArticlesPreview/${articleId}`]);
  }

  editArticle(articleId: string): void {
    this.router.navigate([`/editArticle/${articleId}`]);
  }

  goBack(): void {
    this.router.navigate(['/articles']);
  }
}
