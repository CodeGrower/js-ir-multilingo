import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticlesService } from '../../articles.service';
import { ArticleUpdateDTO } from '../../models/ArticleUpdateDTO';
import { NotificationService } from '../../../core/services/notification.service';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../../core/services/internationalization.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-my-articles-edit',
  templateUrl: './my-articles-edit.component.html',
  styleUrls: ['./my-articles-edit.component.css']
})
export class MyArticlesEditComponent implements OnInit {
  public articleId: string;
  public currentArticle: ArticleUpdateDTO = {id: '', content: '', title: '', articleId: ''};
  public newContent: string;
  public newTitle: string;
  public articleForm: FormGroup;
  public originalArticleId: string;
  public articleLanguage: string;
  public languageSubscription: Subscription;
  public uiComponents = {
    Title: '',
    Content: '',
    SaveChanges: '',
    GoBack: '',
    UpdateArticle: '',
    UpdateArticleFail: '',
    ContentInfo: '',
    InputInfo: '',
  };

  constructor(
    private readonly route: ActivatedRoute,
    private readonly articleService: ArticlesService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
    private readonly internationalizationService: InternationalizationService,
    private readonly fb: FormBuilder,
  ) {
    this.route.data.subscribe(({article}) => {
      this.currentArticle = article;
    });

    this.articleForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(10)]],
      content: ['', [Validators.required, Validators.minLength(50)]],
    });
  }
  ngOnInit() {
    // tslint:disable-next-line: no-string-literal
    this.articleId = this.route.snapshot.params['id'];
    this.internationalizationService.getArticle(this.articleId).subscribe(article => {
      this.currentArticle = article;
      this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
        this.internationalizationService.translateAllComponents(lang).subscribe((components) => {
          this.internationalizationService.swap(this.uiComponents, components);
        });
      });
    });
  }
  editArticle(id: string): void {
    const body: any = {
      title: this.newTitle,
      content: this.newContent,
      articleId: id,
    };
    this.articleService.updateArticleVersion(id, body).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.UpdateArticle}`);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.UpdateArticleFail}`);
    });
    this.goBack();
  }

  goBack(): void {
    this.router.navigate(['/articles']);
  }

}
