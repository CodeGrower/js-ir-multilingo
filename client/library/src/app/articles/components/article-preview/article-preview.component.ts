import { Component, OnInit, OnDestroy } from '@angular/core';
import { ArticleDTO } from '../../models/article.dto';
import { ActivatedRoute } from '@angular/router';
import { ArticlesService } from '../../articles.service';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../../core/services/internationalization.service';
import { CONFIG } from '../../../config/config';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RateDTO } from '../../models/rate.dto';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-article-preview',
  templateUrl: './article-preview.component.html',
  styleUrls: ['./article-preview.component.css']
})
export class ArticlePreviewComponent implements OnInit, OnDestroy {
  public currentArticle: ArticleDTO = {
    title: '', content: '', author: '', authorImgUrl: '', imgUrl: '', updatedOn: '', rating: 0, rateCookies: {}
  };
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
    CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;
  public form: FormGroup;
  public key: number;
  public articleId: string;
  public rate: number;
  public rating = 0;
  public languageSubscription: Subscription;
  public uiComponents = {
    ArticleTitle: '',
    ArticleContent: '',
    ChooseFile: '',
    Create: '',
    AvgRate: '',
    LastUpdate: '',
    RateThanks: '',
  };
  private currentCookie: string;
  private lang: string;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly articleService: ArticlesService,
    private fb: FormBuilder,
    private readonly cookieService: CookieService,
    private readonly notificationService: NotificationService,
    private readonly internationalizationService: InternationalizationService,
  ) {
    this.articleService.getIpAddress().subscribe(data => {
      this.cookieService.set('cookie-name', data.ipAddress);
    });
    this.currentCookie = this.cookieService.get('cookie-name');
    this.route.data.subscribe(({components, article}) => {
      this.currentArticle = article;
      if (Object.keys(this.currentArticle.rateCookies).length > 0) {
        let bool = true;
        for (const key in this.currentArticle.rateCookies) {
          if (this.currentArticle.rateCookies.hasOwnProperty(key)) {
            const element = this.currentArticle.rateCookies[key];
            if (element.cookie === this.currentCookie) {
              bool = false;
              this.key = element.rate;
              this.setForm();
            }
          }
        }
        if (bool) {
          this.key = 0;
          this.setForm();
        }
      } else {
        this.key = 0;
        this.setForm();
      }
    });
  }

  ngOnInit() {
    this.articleService.getIpAddress().subscribe(data => {
      this.cookieService.set('cookie-name', data.ipAddress);
    });
    this.currentCookie = this.cookieService.get('cookie-name');
    // tslint:disable-next-line: no-string-literal
    this.articleId = this.route.snapshot.params['id'];
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.lang = lang;
      this.internationalizationService.translateAllComponents(lang).subscribe((components) => {
        this.internationalizationService.swap(this.uiComponents, components);
      });
      this.internationalizationService.getArticleById(this.articleId, lang).subscribe((article) => {
          this.currentArticle = article;
          if (Object.keys(this.currentArticle.rateCookies).length > 0) {
            let bool = true;
            for (const key in this.currentArticle.rateCookies) {
              if (this.currentArticle.rateCookies.hasOwnProperty(key)) {
                const element = this.currentArticle.rateCookies[key];
                if (element.cookie === this.currentCookie) {
                  bool = false;
                  this.key = element.rate;
                  this.setForm();
                }
              }
            }
            if (bool) {
              this.key = 0;
              this.setForm();
            }
          } else {
            this.key = 0;
            this.setForm();
          }
    });
  });
  }

  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
}

  setForm(): void {
    this.form = this.fb.group({
      rating: [this.key, Validators.required],
    });
  }

  rateBook(): void {
    if (this.rating === 0 || !this.rating) {
    } else if (this.rate !== this.rating) {
      this.rate = this.rating;
      const rateObj: RateDTO = {
        rating: this.rate,
        cookie: this.currentCookie,
        content: this.currentArticle.content
      };
      this.articleService.rateTranslation(rateObj, this.lang).subscribe(() => {
          this.notificationService.success(`${this.uiComponents.RateThanks}`);
          this.internationalizationService.getArticleById(this.articleId, this.lang).subscribe(article => {
            this.currentArticle = article;
      });
    });
  }
}
}
