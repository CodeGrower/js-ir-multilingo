import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllArticlesComponent } from './components/all-articles/all-articles.component';
import { ArticlesRoutingModule } from './articles-routing.module';
import { SharedModule } from '../core/shared-module/shared.module';
import { ArticlePreviewComponent } from './components/article-preview/article-preview.component';
import { CreateArticleComponent } from './components/create-article/create-article.component';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MyArticlesComponent } from './components/my-articles/my-articles.component';
import { MyArticlesPreviewComponent } from './components/my-articles-preview/my-articles-preview.component';
import { MyArticlesEditComponent } from './components/my-articles-edit/my-articles-edit.component';

@NgModule({
  declarations: [
    AllArticlesComponent,
    ArticlePreviewComponent,
    CreateArticleComponent,
    MyArticlesComponent,
    MyArticlesPreviewComponent,
    MyArticlesEditComponent,
  ],
  imports: [
    ImageCropperModule,
    NgxStarRatingModule,
    SharedModule,
    ArticlesRoutingModule,
    CommonModule,
  ]
})
export class ArticlesModule { }
