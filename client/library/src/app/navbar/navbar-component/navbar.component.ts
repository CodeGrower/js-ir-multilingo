import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { UserDTO } from '../../users/models/user.dto';
import { CONFIG } from '../../config/config';
import { NavbarService } from '../navbar.service';
import { InternationalizationService } from '../../core/services/internationalization.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  public isCollapsed = true;
  public searchedText = '';
  public supportedLanguages = [];
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
  CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;
  public loggedUserSubscription: Subscription;
  public languageSubscription: Subscription;
  public isCollapsedNav = false;
  public loggedUserData: UserDTO;
  public localeLanguage = navigator.language;
  public uiComponents = {
    SignIn: '',
    Articles: '',
    Register: '',
    Translations: '',
    LogOut: '',
    AdminPanel: '',
    Search: '',
    SucLogOut: '',
    InvalidLogOut: '',
    Profile: '',
    ChooseLanguage: '',
    MyArticles: '',
  };

  constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly navbarService: NavbarService,
    private readonly internationalizationService: InternationalizationService,
    private readonly route: Router) {
  }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => (this.loggedUserData = data)
    );
    this.internationalizationService.getAllLanguages().subscribe();
    this.internationalizationService.allLanguages$.subscribe(data => {
      this.supportedLanguages = data;
    });
    this.internationalizationService.emitLanguage(this.localeLanguage);
    this.internationalizationService.translateAllComponents(this.localeLanguage).subscribe((translatedComponents) => {
      this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  swithcLanguage(): void {

    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((data) => {
            this.internationalizationService.translateAllComponents(data).subscribe((text) => {
              this.internationalizationService.swap(this.uiComponents, text);
            });
          });

    this.languageSubscription.unsubscribe();
  }

  logout(): void {
    this.authService.logout().subscribe(() => {
      this.notificationService.success(`${this.uiComponents.SucLogOut}`);
      this.route.navigate(['/home']);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.InvalidLogOut}`);
    });
  }

  searchForArticle(): void {
    if (this.searchedText) {
    this.route.navigate([`/search/${this.searchedText}`]);
    this.searchedText = '';
    }
  }

  onChangeLanguage(language: string): void {
    this.internationalizationService.emitLanguage(language);
    this.swithcLanguage();
      }
}
