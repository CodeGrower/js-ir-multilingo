import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  public language: string;
  constructor(
    private readonly http: HttpClient,
    ) {
  }

      public getComponentsByLang(text: string, language) {
        return this.http.post<any>(`${CONFIG.DOMAIN_NAME}/ui-components`, text, language);
      }
}
