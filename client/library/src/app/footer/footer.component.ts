import { Component, OnInit, OnDestroy } from '@angular/core';
import { InternationalizationService } from '../core/services/internationalization.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnDestroy {

  public languageSubscription: Subscription;
  public uiComponents = {
    CreatedBy: '',
    Technologies: '',
    ThanksTo: '',
  };

  constructor(
    private readonly internationalizationService: InternationalizationService,
  ) {
  }

  ngOnInit() {
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
    });
  }

  public ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

}
