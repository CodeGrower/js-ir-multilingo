import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { TranslationsRoutingModule } from './translations-routing.module';
import { SharedModule } from '../core/shared-module/shared.module';
import { TranslationPreviewComponent } from './translation-preview/translation-preview.component';



@NgModule({
  declarations: [AllTranslationsComponent, TranslationPreviewComponent],
  imports: [
    SharedModule,
    CommonModule,
    TranslationsRoutingModule
  ]
})
export class TranslationsModule { }
