import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { TranslationPreviewComponent } from './translation-preview/translation-preview.component';
import { AllTranslationsResolver } from '../core/resolvers/getAllTranslations.resolver';

const routes: Routes = [
    { path: '', component: AllTranslationsComponent, pathMatch: 'full', resolve: {
      translations: AllTranslationsResolver
    }},
    { path: ':id', component: TranslationPreviewComponent, pathMatch: 'full'},
  ];

@NgModule({
    declarations: [],
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
  })
export class TranslationsRoutingModule { }

