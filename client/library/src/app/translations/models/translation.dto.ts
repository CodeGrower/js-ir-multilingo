export class TranslationDTO {
    id: string;
    text: string;
    originalLanguage: string;
    targetLanguageCode: string;
    targetLanguage: string;
    translatedText: string;
    isOriginal: boolean;
}
