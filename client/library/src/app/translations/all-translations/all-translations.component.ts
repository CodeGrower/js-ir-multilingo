import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslationsService } from '../translations.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationDTO } from '../models/translation.dto';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';

@Component({
  selector: 'app-all-translations',
  templateUrl: './all-translations.component.html',
  styleUrls: ['./all-translations.component.css']
})
export class AllTranslationsComponent implements OnInit, OnDestroy {
  public translations: TranslationDTO[];
  public languageSubscription: Subscription;
  public p = 1;
  public uiComponents = {
    Translations: '',
    TranslatedText: '',
    Text: '',
    TranslationLanguage: '',
    EditTranslation: '',
  };
  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly internationalizationService: InternationalizationService,
    ) {
      this.route.data.subscribe(({translations}) => this.translations = translations);
     }

  ngOnInit() {
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
    });
  }

  ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

  editTranslation(translationId: string): void {
    this.router.navigate([`translations/${translationId}`]);
  }
}
