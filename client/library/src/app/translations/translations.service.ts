import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../config/config';
import { TranslationDTO } from './models/translation.dto';

@Injectable({
  providedIn: 'root'
})
export class TranslationsService {

  constructor(private readonly http: HttpClient) { }


  getTranslationById(translationId: string): Observable<TranslationDTO> {
    return this.http.get<TranslationDTO>(`${CONFIG.DOMAIN_NAME}/translations/${translationId}`);
  }

  editTranslation(translatedText: string, translationId: string): Observable<TranslationDTO> {
    const translation = {
      translatedText
    };
    return this.http.put<TranslationDTO>(`${CONFIG.DOMAIN_NAME}/translations/${translationId}/edit`, translation);
  }

}
