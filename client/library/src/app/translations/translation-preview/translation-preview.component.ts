import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslationDTO } from '../models/translation.dto';
import { TranslationsService } from '../translations.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../core/services/notification.service';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';

@Component({
  selector: 'app-translation-preview',
  templateUrl: './translation-preview.component.html',
  styleUrls: ['./translation-preview.component.css']
})
export class TranslationPreviewComponent implements OnInit, OnDestroy {
  public currentTranslation: TranslationDTO;
  public languageSubscription: Subscription;
  public uiComponents = {
    OriginalText: '',
    TranslatedText: '',
    SaveChanges: '',
    ArticleUpdated: '',
  };
  constructor(
    private readonly translationsService: TranslationsService,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly internationalizationService: InternationalizationService,
  ) { }

  ngOnInit() {
    // tslint:disable-next-line:no-string-literal
    const translationId: string = this.route.snapshot.params['id'];
    this.translationsService.getTranslationById(translationId).subscribe(article => this.currentTranslation = article);
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
    });
  }

  public ngOnDestroy(): void {
    this.languageSubscription.unsubscribe();
  }

  editTranslation(translatedText: string) {
    this.translationsService.editTranslation(translatedText, this.currentTranslation.id).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.ArticleUpdated}`);
      this.router.navigate(['/translations']);
    });
  }
}
