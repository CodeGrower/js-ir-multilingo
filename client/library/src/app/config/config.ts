const DOMAIN_NAME = 'http://localhost:3000';
const USER_AVATAR_SRC_PREFIX = `${DOMAIN_NAME}/avatars/`;
const DEFAULT_USER_AVATAR_IMAGE_PATH = './../../assets/default-user.png';
export const CONFIG = {
  DOMAIN_NAME,
  USER_AVATAR_SRC_PREFIX,
  DEFAULT_USER_AVATAR_IMAGE_PATH,
};

