import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './core/shared-module/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navbar/navbar-component/navbar.component';
import { CoreModule } from './core/core.module';
import { ToastrModule } from 'ngx-toastr';
import { AuthInterceptor } from './auth/interseptors/auth-interceptor';
import { UnAuthInterceptor } from './auth/interseptors/unauthorized-interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { ArticlesModule } from './articles/articles.module';
import { FooterComponent } from './footer/footer.component';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SpinnerInterceptor } from './auth/interseptors/spinner.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
  ],
  imports: [
    ArticlesModule,
    BrowserModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    CoreModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnAuthInterceptor,
      multi: true
    },
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
