import { OnInit, Component, OnDestroy } from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { LoginUserDTO } from '../../users/models/login-user.dto';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginUsername: string;
  public loginPassword: string;
  public loginForm: FormGroup;
  public languageSubscription: Subscription;
  public uiComponents = {
    SignIn: '',
    Username: '',
    Password: '',
    EnterUsername: '',
    EnterPassword: '',
    InvalidUP: '',
    SucLogIn: '',
    UsernameInfo: '',
    PasswordInfo: '',
  };
  constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly route: Router,
    private readonly router: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly internationalizationService: InternationalizationService,
    public http: HttpClient) {
      this.loginForm = this.fb.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required]],
      });
      this.router.data.subscribe(({components}) => {
        this.internationalizationService.swap(this.uiComponents, components);
      });

     }
  ngOnInit() {
    this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
      this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
        this.internationalizationService.swap(this.uiComponents, translatedComponents);
      });
    });
  }
  ngOnDestroy() {
    this.languageSubscription.unsubscribe();
  }

  login(): void {
    const body: LoginUserDTO = {
      username: this.loginUsername,
      password: this.loginPassword
    };
    this.authService.login(body).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.SucLogIn}`);
      this.route.navigate(['/home']);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.InvalidUP}`);
    });
  }
  close(): void {
    this.route.navigate(['/home']);
}

}
