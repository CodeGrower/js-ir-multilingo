import { AuthService } from './auth.service';
import { TestBed } from '@angular/core/testing';

import { HomeComponent } from '../home/home-component/home.component';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../core/services/storage.service';
import { combineLatest, of } from 'rxjs';
import { LoginUserDTO } from '../users/models/login-user.dto';
import { CreateUserDTO } from '../users/models/create-user.dto';

describe('AuthService', () => {
    const http = {
        post() {},
        delete() {}
      };
    const storage = {
        getItem() { return ''; },
        setItem() { },
        removeItem() { }
      };

    let getService: () => AuthService;

    beforeEach(() => {
        // clear all spies and mocks
        jest.clearAllMocks();

        TestBed.configureTestingModule({
          imports: [],
          declarations: [HomeComponent],
          providers: [
            AuthService,
            HttpClient,
            StorageService,
          ]
        })
        .overrideProvider(HttpClient, { useValue: http })
        .overrideProvider(StorageService, { useValue: storage });

        getService = () => TestBed.get(AuthService);
      });
    it('should handle correctly invalid (or empty/none) tokens', (done) => {

    // tslint:disable-next-line: max-line-length
    const token  = 'invalid.token';

    const spy = jest.spyOn(storage, 'setItem').mockImplementation(() => token);

    const service = getService();

    service.loggedUserData$.subscribe(
      (loggedUser) => {
        expect(loggedUser).toBe(null);
        done();
      },
    );

  });

    it('should correctly initialize state with a valid token', (done) => {

    // tslint:disable-next-line: max-line-length
    const token  = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const spy = jest.spyOn(storage, 'getItem').mockImplementation(() => token);

    const service = getService();

    service.loggedUserDataSubject$.subscribe(
      (loggedUser) => {
        expect(loggedUser).toBeDefined();
        done();
      },
    );

  });

    it('login() should call http.post with the correct parameters', () => {

    // tslint:disable-next-line: max-line-length
    const token  = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const spy = jest.spyOn(http, 'post').mockImplementation(() => of({token}));

    const service = getService();
    const user = new LoginUserDTO();

    service.login(user);

    expect(http.post).toHaveBeenCalledTimes(1);
    expect(http.post).toHaveBeenLastCalledWith(`http://localhost:3000/session`, user);

  });

    it('login() should call storage.setItem', (done) => {

    // tslint:disable-next-line: max-line-length
    const token  = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const postSpy = jest.spyOn(http, 'post').mockImplementation(() => of({token}));
    const saveSpy = jest.spyOn(storage, 'setItem');

    const service = getService();
    const user = new LoginUserDTO();

    service.login(user).subscribe(
      () => {
        expect(storage.setItem).toHaveBeenCalledTimes(1);

        done();
      }
    );

  });

    it('logout() should call storage.removeItem', () => {

    const postSpy = jest.spyOn(http, 'delete').mockImplementation(() => of({}));
    const saveSpy = jest.spyOn(storage, 'removeItem');

    const service = getService();

    service.logout();

    expect(storage.removeItem).toHaveBeenCalledWith('token');

  });

    it('logout() should set the correct state', (done) => {

    const saveSpy = jest.spyOn(storage, 'removeItem').mockImplementation(() => {});

    const service = getService();

    service.logout();

    service.loggedUserDataSubject$.subscribe(
      (loggedUser) => {
        expect(loggedUser).toBe(null);

        done();
      },
    );

  });

    it('register() should call http.post', () => {

    const user = new CreateUserDTO();
    const spy = jest.spyOn(http, 'post');

    const service = getService();

    service.register(user);

    expect(http.post).toHaveBeenCalledWith(`http://localhost:3000/users`, user);

  });

});
