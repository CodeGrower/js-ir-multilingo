import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateUserDTO } from '../../users/models/create-user.dto';
import { NotificationService } from '../../core/services/notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs';
import { InternationalizationService } from '../../core/services/internationalization.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  public regUsername: string;
  public regName: string;
  public regSurname: string;
  public regEmail: string;
  public regPassword: string;
  public registerForm: FormGroup;
  public languageSubscription: Subscription;
  public uiComponents = {
    Register: '',
    Username: '',
    Name: '',
    Surname: '',
    Email: '',
    Password: '',
    EnterUsername: '',
    EnterName: '',
    EnterSurname: '',
    EnterEmail: '',
    EnterPassword: '',
    SucReg: '',
    InvalidC: '',
    UsernameInfo: '',
    NameInfo: '',
    SurnameInfo: '',
    EmailInfo: '',
    PasswordInfo: '',
  };
  constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly route: Router,
    private readonly router: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly internationalizationService: InternationalizationService,
    ) {

      this.registerForm = this.formBuilder.group({
        username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        surname: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
      });
      this.router.data.subscribe(({components}) => {
        this.internationalizationService.swap(this.uiComponents, components);
      });
    }

    ngOnInit() {
      this.languageSubscription = this.internationalizationService.getLanguage().subscribe((lang) => {
        this.internationalizationService.translateAllComponents(lang).subscribe((translatedComponents) => {
          this.internationalizationService.swap(this.uiComponents, translatedComponents);
        });
      });
    }

    ngOnDestroy(): void {
      this.languageSubscription.unsubscribe();
    }

  register(): void {
    const body: CreateUserDTO = {
      username: this.regUsername,
      name: this.regName,
      surname: this.regSurname,
      email: this.regEmail,
      password: this.regPassword
    };

    this.authService.register(body).subscribe(() => {
      this.notificationService.success(`${this.uiComponents.SucReg}`);
      this.route.navigate(['/users/login']);
    },
    () => {
      this.notificationService.error(`${this.uiComponents.InvalidC}`);
    });
  }

  close(): void {
    this.route.navigate(['/home']);
  }
}
