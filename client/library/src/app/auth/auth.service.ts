import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { StorageService } from '../core/services/storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CreateUserDTO } from '../users/models/create-user.dto';
import { CONFIG } from '../config/config';
import { UserDTO } from '../users/models/user.dto';
import { LoginUserDTO } from '../users/models/login-user.dto';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(
    this.getUserDataIfAuthenticated()
  );

  public get loggedUserData$(): Observable<UserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }
  public emitUserData(user: UserDTO): void {
    this.loggedUserDataSubject$.next(user);
  }

  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    ) { }

  public register(body: CreateUserDTO): any {
    return this.http.post<any>(`${CONFIG.DOMAIN_NAME}/users`, body);
  }

  public login(body: LoginUserDTO): Observable<any> {
    return this.http.post<any>(`${CONFIG.DOMAIN_NAME}/session`, body)
        .pipe(
          tap(({ access_token }) => {
            this.storageService.setItem('token', access_token);
            const userData: UserDTO = helper.decodeToken(access_token);
            this.emitUserData(userData);
          })
        );
  }

  public logout(): Observable<any> {
    return this.http.delete<any>(`${CONFIG.DOMAIN_NAME}/session`).
    pipe(
      tap(() => {
        this.loggedUserDataSubject$.next(null);
        this.storageService.removeItem('token');
      })
    );
  }

  public getUserDataIfAuthenticated(): UserDTO {
    const token: string = this.storageService.getItem('token');

    if (token && helper.isTokenExpired(token)) {
      this.storageService.removeItem('token');
      return null;
    }
    return token ? helper.decodeToken(token) : null;
  }
}
