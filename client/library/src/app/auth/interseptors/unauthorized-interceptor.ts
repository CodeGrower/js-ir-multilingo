import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';

@Injectable()
export class UnAuthInterceptor implements HttpInterceptor {
  public constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly route: Router,
  ) {}

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: any) => {
        if (error.status === 401) {
            this.authService.logout().subscribe(() => {
                this.notificationService.success('Your session has expired ! You have to login !');
                this.route.navigate(['/home']);
              },
              () => {
                this.notificationService.error('Invalid logout!');
              });
        }
        return throwError(error);
      })
    );
  }
}
