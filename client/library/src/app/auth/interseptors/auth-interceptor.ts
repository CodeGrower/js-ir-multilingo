import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';


import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    public constructor(private readonly storage: StorageService) {}

    public intercept(
      request: HttpRequest<any>,
      next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const token = this.storage.getItem('token');
        let updatedRequest: any;
        if (request.url.indexOf('imgur') > -1) {
          updatedRequest = request.clone({
            headers: request.headers.set('Authorization', 'Client-ID 27eb053ac8b2181')
          });
        } else {
          updatedRequest = request.clone({
          headers: request.headers.set('Authorization', `Bearer ${token}`)
        });
      }
        return next.handle(updatedRequest);
  }
}
