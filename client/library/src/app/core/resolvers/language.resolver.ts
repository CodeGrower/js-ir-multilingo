import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { take, switchMap, first } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()

export class LanguageResolver implements Resolve<object> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
    ) {}

    resolve(): Observable<object> {
   return this.internationalizationService.getLanguage().pipe(
       switchMap((value) => this.internationalizationService.translateAllComponents(value)),
       first()
   );
}
}
