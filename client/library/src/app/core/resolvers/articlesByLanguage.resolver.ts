import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { take, switchMap, first } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()

export class ArticlesByLanguage implements Resolve<object> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
    ) {}
resolve(): Observable<object> {
    return this.internationalizationService.getLanguage().pipe(
        switchMap((value) => this.internationalizationService.getAllArticlesByLangCode(value)),
        first()
    );
}
}
