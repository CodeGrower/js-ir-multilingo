import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { Observable } from 'rxjs';

@Injectable()

export class AllTranslationsResolver implements Resolve<any> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
    ) {}

    resolve(): Observable<any> {
        return this.internationalizationService.getAllTranslations();
    }
}
