import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { Observable } from 'rxjs';
import { switchMap, first, delay } from 'rxjs/operators';

@Injectable()

export class LastThreeArticlesResolver implements Resolve<any> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
    ) {}

    resolve(): Observable<any> {
        return this.internationalizationService.getLanguage().pipe(
            delay(100),
            switchMap((value) => {
                return this.internationalizationService.getLastThreeArticles(value);
            }),
            first()
        );
    }
}
