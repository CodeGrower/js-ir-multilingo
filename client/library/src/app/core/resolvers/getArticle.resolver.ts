import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class ArticleResolver implements Resolve<any> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
        private http: HttpClient,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        // tslint:disable-next-line: no-string-literal
        const id = route.params['id'];
        return this.internationalizationService.getArticle(id);
    }
}
