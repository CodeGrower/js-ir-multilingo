import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { take, switchMap, first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class ArticleByIdResolver implements Resolve<any> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
        private http: HttpClient,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        // tslint:disable-next-line: no-string-literal
        const id = route.params['id'];
        return this.internationalizationService.getLanguage().pipe(
            switchMap((value) => this.internationalizationService.getArticleById(id, value)),
            first()
            );
    }
}
