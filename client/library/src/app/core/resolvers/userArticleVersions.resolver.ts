import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { InternationalizationService } from '../services/internationalization.service';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()

export class UsersArticleVersionsResolver implements Resolve<any> {
    constructor(
        private readonly internationalizationService: InternationalizationService,
    ) {}

    resolve(): Observable<any> {
        return this.internationalizationService.getAllUserArticleVersions();
    }
}
