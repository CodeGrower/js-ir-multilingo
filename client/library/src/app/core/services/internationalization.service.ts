import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../../config/config';
import { tap, map } from 'rxjs/operators';
import { ArticleDTO } from '../../articles/models/article.dto';
import { UserDTO } from '../../users/models/user.dto';
import { TranslationDTO } from '../../translations/models/translation.dto';
import { LanguageDTO } from '../../users/models/language-dto';

@Injectable({
  providedIn: 'root'
})
export class InternationalizationService {

  constructor(
    private readonly http: HttpClient,
  ) {  }

  public selectedLanguage$ = new BehaviorSubject<string>(
    navigator.language
  );

  public translatedComponents$ = new BehaviorSubject<object> (null);

  public allArticlesByLanguage$ = new BehaviorSubject<object> (null);

  public allUserArticleVersions$ = new BehaviorSubject<object> (null);

  public lastThreeArticles$ = new BehaviorSubject<object> (null);

  public selectArticleId$ = new BehaviorSubject<string> (null);

  public getAllUsers$ = new BehaviorSubject<object> (null);

  public allTranslations$ = new BehaviorSubject<object> (null);

  public articleById$ = new BehaviorSubject<object> (null);

  public getArticle$ = new BehaviorSubject<object> (null);

  private allLanguagesSubject$ = new BehaviorSubject<LanguageDTO[]> (null);

  public getAllArticleVersions$ = new BehaviorSubject<object> (null);

  public get allLanguages$() {
    return this.allLanguagesSubject$.asObservable().pipe(
      map(data => data ? data.map(el => el.language) : [])
    );
  }

  public emitLanguage(language: string): void {
    this.selectedLanguage$.next(language);
  }

  public getLanguage(): Observable<string> {
    return this.selectedLanguage$.asObservable();
  }

  public translateAllComponents(language: string) {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/ui-components/${language}`).pipe(
      tap((components) => {
        this.translatedComponents$.next(components);
      })
    );
  }

  public getAllArticlesByLangCode(language: string) {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/articles?language=${language}`).pipe(
      tap((articles) => {
        this.allArticlesByLanguage$.next(articles);
      })
    );
  }

  public getLastThreeArticles(langCode: string) {
    return this.http.get<ArticleDTO[]>(`${CONFIG.DOMAIN_NAME}/articles/lastThree?language=${langCode}`)
    .pipe(
      tap((articles) => {
        this.lastThreeArticles$.next(articles);
      })
    );
  }

  public getAllUserArticleVersions() {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/article-version/user/articles/versions`).pipe(
      tap((articles) => {
        this.allUserArticleVersions$.next(articles);
      })
    );
  }

  public getArticleById(articleId: string, language: string): Observable<ArticleDTO> {
    return this.http.get<ArticleDTO>(`${CONFIG.DOMAIN_NAME}/articles/${articleId}/${language}`).pipe(
      tap((article) => {
        this.articleById$.next(article);
      })
    );
  }

  public getArticle(artId: string) {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/article-version/${artId}`).pipe(
      tap((article) => {
        this.getArticle$.next(article);
      })
    );
  }

  public getAllUsers() {
    return this.http.get<UserDTO[]>(`${CONFIG.DOMAIN_NAME}/users`).pipe(
      tap((users) => {
        this.getAllUsers$.next(users);
      })
    );
  }

  public getAllTranslations() {
    return this.http.get<TranslationDTO[]>(`${CONFIG.DOMAIN_NAME}/translations`).pipe(
      tap((translations) => {
        this.allTranslations$.next(translations);
      })
    );
  }
  public getAllLanguages() {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/ui-components`).pipe(
      tap((languages) => {
        this.allLanguagesSubject$.next(languages);
      })
    );
  }
  public updateAllLanguages(languages: LanguageDTO[]) {
    this.allLanguagesSubject$.next(languages);
  }

  public getAllArticleVersions(artId: string) {
    return this.http.get<any>(`${CONFIG.DOMAIN_NAME}/article-version/${artId}/versions`).pipe(
      tap((allVersions) => {
        this.getAllArticleVersions$.next(allVersions);
      })
    );
  }

  public swap(a: object, b: object) {
    // tslint:disable-next-line: forin
    for (const key in a) {
        a[key] = b[key];
    }
    return a;
}

}
