import { TestBed } from '@angular/core/testing';

import { StorageService } from './storage.service';

describe('StorageService', () => {

  let service: StorageService;

  beforeEach(() => {

    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [StorageService],
    });

    service = TestBed.get(StorageService);
  });

  it('setItem should call localStorage.setItem', () => {
    // Arrange
     const mock = 'mockData';
     const spy = jest.spyOn(localStorage, 'setItem').mockImplementation(() => {});

     // Act
     service.setItem('test', mock);

     // Assert
     expect(localStorage.setItem).toHaveBeenCalledWith('test', mock);

  });

  it('getItem should call localStorage.getItem', () => {
    // Arrange
    const spy = jest.spyOn(localStorage, 'getItem').mockImplementation(() => '');

    // Act
    service.getItem('test');

    // Assert
    expect(localStorage.getItem).toHaveBeenCalledWith('test');

  });

  it('removeItem should call localStorage.removeItem', () => {
    // Arrange
    const spy = jest.spyOn(localStorage, 'removeItem').mockImplementation(() => {});

    // Act
    service.removeItem('test');

    // Assert
    expect(localStorage.removeItem).toHaveBeenCalledWith('test');

  });

  it('clear should call localStorage.clear', () => {
    // Arrange
    const spy = jest.spyOn(localStorage, 'clear').mockImplementation(() => {});

    // Act
    service.clear();

    // Assert
    expect(localStorage.clear).toHaveBeenCalledTimes(1);

  });

});
