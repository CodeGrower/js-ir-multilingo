import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../users/models/user.dto';


@Injectable({
  providedIn: 'root'
})

export class RoleService {
    private loggedUserSubscription: Subscription;
    public loggedUserData: UserDTO;
    public isTrue: boolean;
    constructor(private readonly authService: AuthService) {

    }
    public isAdmin(): boolean {
        this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
            (data: UserDTO) => (this.loggedUserData = data)
          );
        if (this.loggedUserData.roles.includes('Admin')) {
          this.isTrue = true;
          } else {
            this.isTrue = false;
          }
        return this.isTrue;
    }
}
