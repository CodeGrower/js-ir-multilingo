import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, NgSelectOption } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatIconModule} from '@angular/material/icon';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
    declarations: [],
    imports: [CommonModule],
    exports: [
      NgxPaginationModule,
      MatIconModule,
      NgbModule,
      FormsModule,
      CommonModule,
      ReactiveFormsModule,
  ]
})
export class SharedModule {}
