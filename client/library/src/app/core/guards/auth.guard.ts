import { CanActivate, Router } from '@angular/router';
import { NotificationService } from '../services/notification.service';
import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
      private readonly authService: AuthService,
      private readonly router: Router,
      private readonly notificationService: NotificationService
    ) {}

    public canActivate(): boolean {
        const user = this.authService.getUserDataIfAuthenticated();
        if (user) {
            return true;
        }
        this.notificationService.error('You have to be logged !');
        this.router.navigate(['users/login']);
    }
}
