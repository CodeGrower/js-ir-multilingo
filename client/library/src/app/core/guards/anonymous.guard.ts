import { CanActivate, Router } from '@angular/router';


import { NotificationService } from '../services/notification.service';
import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class AnonymousGuard implements CanActivate {
    constructor(
      private readonly authService: AuthService,
      private readonly router: Router,
      private readonly notificationService: NotificationService
    ) {}

    public canActivate(): boolean {
        const user = this.authService.getUserDataIfAuthenticated();
        if (user) {
            this.notificationService.error('You are logged in!');
            this.router.navigate(['home']);
        }
        return true;
    }
}
