import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NotificationService } from './services/notification.service';
import { AuthService } from '../auth/auth.service';
import { StorageService } from './services/storage.service';
import { AuthGuard } from './guards/auth.guard';
import { AnonymousGuard } from './guards/anonymous.guard';
import { LanguageResolver } from './resolvers/language.resolver';
import { ArticlesByLanguage } from './resolvers/articlesByLanguage.resolver';
import { UsersArticleVersionsResolver } from './resolvers/userArticleVersions.resolver';
import { AllUsersResolver } from './resolvers/getAllUsers.resolver';
import { AllTranslationsResolver } from './resolvers/getAllTranslations.resolver';
import { ArticleByIdResolver } from './resolvers/articleById.resolver';
import { ArticleResolver } from './resolvers/getArticle.resolver';
import { AllArticleVersionsResolver } from './resolvers/getAllArticleVersions.resolver';
import { LastThreeArticlesResolver } from './resolvers/getLastThreeArticles.resolver';

@NgModule({
  providers: [
    NotificationService, AuthService, StorageService, AuthGuard, AnonymousGuard,
     LanguageResolver, ArticlesByLanguage, UsersArticleVersionsResolver, AllUsersResolver,
      AllTranslationsResolver, ArticleByIdResolver, ArticleResolver, AllArticleVersionsResolver,
    LastThreeArticlesResolver]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
