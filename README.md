# MultiLingo App 

### Project Description

MultiLingo is text translation app which could be used to show the content of a web site (for example a knowledge sharing portal/wiki) in multiple languages. 

---

### Getting Started

These instructions will get you a copy of the project and allow you to run it on your local machine for development and testing purposes.

#### Server

After you clone successfully this repository:

- navigate to the `server` folder

- create a **database** in MySQL Workbench. The default name is multiLingodb and if you would like to rename it it, you must change it the .env config file

- create `.env` file at root level- it contains sensitive data about your server. DB_USERNAME and DB_PASSWORD are the ones set in your MySQL Workbench

```sh
PORT=3000
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=1234
DB_DATABASE_NAME=multiLingodb
GOOGLE_APPLICATION_CREDENTIALS=src/config/google-api.json
```

- create ormconfig.json file at root level

```sh
{
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "1234",
    "database": "multiLingodb",
    "synchronize": true,
    "logging": false,
    "entities": [
      "src/data/enitites/*.ts" 
    ],
    "migrations": [
      "src/data/migration/*.ts"
    ],
    "cli": {
      "entitiesDir": "src/data/entities",
      "migrationsDir": "src/data/migration"
    }
  }
```

- open the terminal or bash at root level of the server and run the following commands:

  ```sh
  $ npm install
  ```

- to populate the database run that command and wait about 10 seconds:
  
  ```sh
  $ npm run seed 
  ```
- to run the server: 
  ```sh
  $ npm run start:dev
  ```
- testing:
  ```sh
  $ npm run test
  ```

#### Client

##### Having successfully run the server, you can run the application

- navigate to the `client` folder

- open the terminal and run the following commands:

  ```sh
  $ npm install
  ```

  ```sh
  $ ng serve --o
  ```

  ```sh
  You can either create your own users, or just login with the existing ones:
  - username: John, password: John123
  ```

---

#### Known Issues

- Website content translated by your locale(browser language) !
- You can change that by use choose language dropdown !

---

#### Testing


```sh
$ npm run test
```

---

### Technologies

- Angular 8
- NestJS
- Bootstrap
- TypeORM
- Google Translate Api
---

### Authors and Contributors

- [Ivan Shterev](https://gitlab.com/IvanShterev)
- [Ivailo Rashevski](https://gitlab.com/ivo92)

---

### License

This project is licensed under the **[MIT](https://choosealicense.com/licenses/mit/)** License
